/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vcm_member20.gcm;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.vcm_member20.R;
import com.vcm_member20.activity.DetilHotNewsActivity;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    private boolean islogin;

    private SharedPreferences sharedPreferences;

    private int notifundangan, notifkabar;

    public void loadPreferences() {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            islogin = sharedPreferences.getBoolean("islogin", false);
        } else {
            islogin = false;
        }
    }

    public void loadSetting()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean checkBoxDiskon = prefs.getBoolean("set_notif_undangan", true);
        boolean checkBoxKabar = prefs.getBoolean("set_notif_kabar", true);

        if(checkBoxDiskon)
        {
            notifundangan = 1;
        }
        else
        {
            notifundangan = 0;
        }

        if(checkBoxKabar)
        {
            notifkabar = 1;
        }
        else
        {
            notifkabar = 0;
        }
    }

    /**
     * Dipanggil ketika menerima message.
     *
     * @param from Sender ID dari pengirim
     * @paramdata data bundle yang mengandung key dan value berdasarkan.
     * data yang dikirimkan oleh GCM */

    @Override
    public void onMessageReceived(String from, Bundle b) {
        //Mengambil data sesuai parameter yang dikirimkan webservice
        loadPreferences();
        loadSetting();

        String param = b.getString("param");

        Log.d(TAG, "param: " + param);

        String sender = b.getString("sender");

        if(param.equalsIgnoreCase("news")) {
            String id = b.getString("idnews");
            String judul = b.getString("judul");
            String isi = b.getString("isi");
            String create = b.getString("create");
            String photo = b.getString("photo");
            String merchant = b.getString("merchant");

            Log.d(TAG, "id: " + id);
            Log.d(TAG, "photo: " + photo);

            if(islogin && notifkabar == 1) {
                sendNotificationBerita(id, judul, isi, create, photo, merchant);
            }
        }
        else if(param.equalsIgnoreCase("promo")) {
            String id = b.getString("idnews");
            String judul = b.getString("judul");
            String isi = b.getString("isi");
            String create = b.getString("create");
            String photo = b.getString("photo");
            String merchant = b.getString("merchant");

            Log.d(TAG, "id: " + id);
            Log.d(TAG, "photo: " + photo);

            if(islogin && notifkabar == 1) {
                sendNotificationPromo(id, judul, isi, create, photo, merchant);
            }
        }

        Log.d(TAG, "from: " + from);
        Log.d(TAG, "sender: " + sender);
    }
    /**
     * Membuat dan menampilkan notifikasi sederhana yang mengandung pesan GCM.
     */

    private void sendNotificationBerita(String id, String judul, String isi, String create, String photo, String merchant) {
        Intent intent = new Intent(this, DetilHotNewsActivity.class);

        Bundle b = new Bundle();
        b.putString("idnews", id);
        b.putString("judul", judul);
        b.putString("isi", isi);
        b.putString("create", create);
        b.putString("photo", photo);
        b.putString("merchant", merchant);

        intent.putExtras(b);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_merchant)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo_merchant))
                .setContentTitle("Berita Terbaru")
                .setContentText(judul)
                .setAutoCancel(true)
                .setTicker(judul)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify((int) System.currentTimeMillis(), notificationBuilder.build());

    }

    private void sendNotificationPromo(String id, String judul, String isi, String create, String photo, String merchant) {
        Intent intent = new Intent(this, DetilHotNewsActivity.class);

        Bundle b = new Bundle();
        b.putString("idnews", id);
        b.putString("judul", judul);
        b.putString("isi", isi);
        b.putString("create", create);
        b.putString("photo", photo);
        b.putString("merchant", merchant);

        intent.putExtras(b);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_merchant)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo_merchant))
                .setContentTitle("Promo Terbaru")
                .setContentText(judul)
                .setAutoCancel(true)
                .setTicker(judul)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify((int) System.currentTimeMillis(), notificationBuilder.build());

    }

}

package com.vcm_member20.model;

import java.util.ArrayList;

/**
 * Created by Exoguru on 23/01/2015.
 */

public class ResultAPI<T> {
    private Boolean status;
    private String msg;
    private ArrayList<T> isi = new ArrayList<T>();


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<T> getIsi() {
        return isi;
    }

    public void setIsi(ArrayList<T> isi) {
        this.isi = isi;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}

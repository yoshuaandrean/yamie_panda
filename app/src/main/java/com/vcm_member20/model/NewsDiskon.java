package com.vcm_member20.model;

/**
 * Created by Exoguru on 23/01/2015.
 */
public class NewsDiskon {
    private String kategori;
    private String id;
    private String judul;
    private String isi;
    private String status;
    private String create_at;
    private String update_at;
    private String id_merchant;
    private String photo;
    private String broadcast;
    private String tgl_star;
    private String tgl_expired;
    private String diskon;
    private String harga;
    private String tgl_create;
    private String tgl_lengkap;


    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    public String getId_merchant() {
        return id_merchant;
    }

    public void setId_merchant(String id_merchant) {
        this.id_merchant = id_merchant;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(String broadcast) {
        this.broadcast = broadcast;
    }

    public String getTgl_star() {
        return tgl_star;
    }

    public void setTgl_star(String tgl_star) {
        this.tgl_star = tgl_star;
    }

    public String getTgl_expired() {
        return tgl_expired;
    }

    public void setTgl_expired(String tgl_expired) {
        this.tgl_expired = tgl_expired;
    }

    public String getDiskon() {
        return diskon;
    }

    public void setDiskon(String diskon) {
        this.diskon = diskon;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getTgl_create() {
        return tgl_create;
    }

    public void setTgl_create(String tgl_create) {
        this.tgl_create = tgl_create;
    }

    public String getTgl_lengkap() {
        return tgl_lengkap;
    }

    public void setTgl_lengkap(String tgl_lengkap) {
        this.tgl_lengkap = tgl_lengkap;
    }
}

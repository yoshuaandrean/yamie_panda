package com.vcm_member20.model;

/**
 * Created by Exoguru on 23/01/2015.
 */
public class Member {
    private String id_member;
    private String nama_member;
    private String email;
    private String telp;
    private String alamat;
    private String photo;
    private String status;
    private String id_qrcode;
    private String image_qrcode;
    private String tgl_lahir;
    private String agama;
    private String email_merchant;

    public String getId_member() {
        return id_member;
    }

    public void setId_member(String id_member) {
        this.id_member = id_member;
    }

    public String getNama_member() {
        return nama_member;
    }

    public void setNama_member(String nama_member) {
        this.nama_member = nama_member;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId_qrcode() {
        return id_qrcode;
    }

    public void setId_qrcode(String id_qrcode) {
        this.id_qrcode = id_qrcode;
    }

    public String getImage_qrcode() {
        return image_qrcode;
    }

    public void setImage_qrcode(String image_qrcode) {
        this.image_qrcode = image_qrcode;
    }

    public String getTgl_lahir() {
        return tgl_lahir;
    }

    public void setTgl_lahir(String tgl_lahir) {
        this.tgl_lahir = tgl_lahir;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getEmail_merchant() {
        return email_merchant;
    }

    public void setEmail_merchant(String email_merchant) {
        this.email_merchant = email_merchant;
    }
}

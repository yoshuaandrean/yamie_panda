package com.vcm_member20.model;

/**
 * Created by Exoguru on 23/01/2015.
 */
public class Katalog {
    private String id_katalog;
    private String produk;
    private String harga;
    private String deskripsi;
    private String status;
    private String photo;
    private String tgl_create;
    private String nama_merchant;

    public String getId_katalog() {
        return id_katalog;
    }

    public void setId_katalog(String id_katalog) {
        this.id_katalog = id_katalog;
    }

    public String getProduk() {
        return produk;
    }

    public void setProduk(String produk) {
        this.produk = produk;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTgl_create() {
        return tgl_create;
    }

    public void setTgl_create(String tgl_create) {
        this.tgl_create = tgl_create;
    }

    public String getNama_merchant() {
        return nama_merchant;
    }

    public void setNama_merchant(String nama_merchant) {
        this.nama_merchant = nama_merchant;
    }
}

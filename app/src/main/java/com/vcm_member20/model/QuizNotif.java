package com.vcm_member20.model;

/**
 * Created by Exoguru on 23/01/2015.
 */
public class QuizNotif {
    private String isi;
    private String tanggal;
    private String judul_kuis;
    private String nama_merchant;


    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getJudul_kuis() {
        return judul_kuis;
    }

    public void setJudul_kuis(String judul_kuis) {
        this.judul_kuis = judul_kuis;
    }

    public String getNama_merchant() {
        return nama_merchant;
    }

    public void setNama_merchant(String nama_merchant) {
        this.nama_merchant = nama_merchant;
    }
}

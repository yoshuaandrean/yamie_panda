package com.vcm_member20.model;

/**
 * Created by Exoguru on 23/01/2015.
 */
public class CheckinMember {

    private String id_member;
    private String nama_member;
    private String tgl_cekin;
    private String nama_cabang;

    public String getNama_member() {
        return nama_member;
    }

    public void setNama_member(String nama_member) {
        this.nama_member = nama_member;
    }

    public String getTgl_cekin() {
        return tgl_cekin;
    }

    public void setTgl_cekin(String tgl_cekin) {
        this.tgl_cekin = tgl_cekin;
    }

    public String getNama_cabang() {
        return nama_cabang;
    }

    public void setNama_cabang(String nama_cabang) {
        this.nama_cabang = nama_cabang;
    }

    public String getId_member() {
        return id_member;
    }

    public void setId_member(String id_member) {
        this.id_member = id_member;
    }
}

package com.vcm_member20.model;

/**
 * Created by Exoguru on 23/01/2015.
 */
public class Share {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

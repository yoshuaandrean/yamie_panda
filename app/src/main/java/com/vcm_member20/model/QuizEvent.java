package com.vcm_member20.model;

/**
 * Created by Exoguru on 23/01/2015.
 */
public class QuizEvent {
    private String id_kuis;
    private String judul_kuis;
    private String pertanyaan;
    private String a;
    private String b;
    private String c;
    private String d;
    private String msg;
    private String photo;
    private String tgl_create;
    private String status;
    private String tgl_selesai;



    public String getId_kuis() {
        return id_kuis;
    }

    public void setId_kuis(String id_kuis) {
        this.id_kuis = id_kuis;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTgl_create() {
        return tgl_create;
    }

    public void setTgl_create(String tgl_create) {
        this.tgl_create = tgl_create;
    }

    public String getJudul_kuis() {
        return judul_kuis;
    }

    public void setJudul_kuis(String judul_kuis) {
        this.judul_kuis = judul_kuis;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTgl_selesai() {
        return tgl_selesai;
    }

    public void setTgl_selesai(String tgl_selesai) {
        this.tgl_selesai = tgl_selesai;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

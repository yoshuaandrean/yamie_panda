package com.vcm_member20.model;

/**
 * Created by Pradana on 31/08/2016.
 */
public class Token {
    private String token;

    /**
     *
     * @return
     * The token
     */
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     * The token
     */
    public void setToken(String token) {
        this.token = token;
    }

}

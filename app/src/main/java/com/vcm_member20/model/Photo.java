package com.vcm_member20.model;

/**
 * Created by Exoguru on 23/01/2015.
 */
public class Photo {
    private String id_katalog;
    private String photo;

    public String getId_katalog() {
        return id_katalog;
    }

    public void setId_katalog(String id_katalog) {
        this.id_katalog = id_katalog;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}

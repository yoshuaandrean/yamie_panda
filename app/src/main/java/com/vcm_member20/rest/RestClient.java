package com.vcm_member20.rest;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vcm_member20.model.Client;
import com.vcm_member20.model.Diskon;
import com.vcm_member20.model.Katalog;
import com.vcm_member20.model.Kategori;
import com.vcm_member20.model.News;
import com.vcm_member20.model.NewsDiskon;
import com.vcm_member20.model.Photo;
import com.vcm_member20.model.Point;
import com.vcm_member20.model.Post;
import com.vcm_member20.model.Result;
import com.vcm_member20.model.ResultAPI;
import com.vcm_member20.model.Results;
import com.vcm_member20.model.Share;
import com.vcm_member20.model.Token;
import com.vcm_member20.model.TokoMerchant;
import com.vcm_member20.model.Update;
import com.vcm_member20.model.User;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public class RestClient {

    private static Context mContext;
    private static Toast toast;
    private static GitApiInterface gitApiInterface ;
    private static String baseUrl = "http://kasir.yamiepanda.com" ;

    public static Retrofit retrofit() {
        OkHttpClient client = httpClient.build();
        return builder.client(client).build();
    }

//    public static GitApiInterface getClient() {
//        if (gitApiInterface == null) {
//
//            Gson gson = new GsonBuilder()
//                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
//                    .create();
//
//            Retrofit client = new Retrofit.Builder()
//                    .baseUrl(baseUrl)
//                    .addConverterFactory(GsonConverterFactory.create(gson))
//                    .build();
//            gitApiInterface = client.create(GitApiInterface.class);
//        }
//        return gitApiInterface ;
//    }

    private static SharedPreferences sharedPref;
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static Gson gson =
            new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create();
    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson));

    public static GitApiInterface getClient(Context context) {
        if(context!=null) {
            mContext = context;
            sharedPref = mContext.getSharedPreferences("shared", Activity.MODE_PRIVATE);
            httpClient.interceptors().add(contentType);
            httpClient.interceptors().add(authentication);
        }
        OkHttpClient client = httpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(GitApiInterface.class);
    }

    private static final Interceptor contentType = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            Request authenticationRequest = originalRequest.newBuilder()
                    .header("Content-Type", "application/javascript")
                    .build();

            Response response = chain.proceed(authenticationRequest);

            return response;
        }
    };

    private static final Interceptor authentication = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            Request authenticationRequest = originalRequest.newBuilder()
                    .header("Authorization", "Bearer " + sharedPref.getString("user_token", ""))
                    .build();

            Response response = chain.proceed(authenticationRequest);

            return response;
        }
    };


    public interface GitApiInterface {


        @FormUrlEncoded
        @POST("/api/v1/auth")
        Call<Result<Token>> getToken(@Field("client_id") int client_id, @Field("email") String  email, @Field("password") String  password);

        @FormUrlEncoded
        @POST("/api/v1/user/register")
        Call<Result<User>> register(@Field("client_id") int client_id,
                                    @Field("email") String  email,
                                    @Field("password") String  password,
                                    @Field("name") String  name,
                                    @Field("phone") String phone,
                                    @Field("address") String address,
                                    @Field("birthday") String birthday,
                                    @Field("religion") String religion);

        @GET("/api/v1/user/myProfile")
        Call<Result<User>> getProfil();

        @FormUrlEncoded
        @PUT("/api/v1/user/updateProfile")
        Call<Result<User>> updateProfil(@Field("name") String name,@Field("phone") String phone,@Field("address") String address,@Field("birthday") String birthday,@Field("religion") String religion, @Field("photo") String photo);

        @FormUrlEncoded
        @PUT("/api/v1/user/updatePassword")
        Call<Result<User>> updatePassword(@Field("password") String password,@Field("password_new") String password_new);

        @FormUrlEncoded
        @POST("/api/v1/forgotPassword")
        Call<Result<User>> forgotPassword(@Field("email") String email);

        @GET("/api/v1/slider")
        Call<Results<Post>> getSlider();

        @GET("/api/v1/news")
        Call<Results<Post>> getNews(@Query("offset") String offset, @Query("limit") String limit);

        @GET("/api/v1/promo")
        Call<Results<Post>> getPromo(@Query("offset") String offset, @Query("limit") String limit);

        @GET("/api/v1/katalog")
        Call<Results<Post>> getKatalog(@Query("offset") String offset, @Query("limit") String limit);

        @GET("/api/v1/katalog")
        Call<Results<Post>> getKatalogCari(@Query("search") String search, @Query("searchFields") String searchFields, @Query("with") String with);

        @GET("/api/v1/client")
        Call<Result<Client>> getProfilMerchant(@Query("with") String with);

        @GET("/api/v1/shared/status")
        Call<Results<Share>> getShare();

        @FormUrlEncoded
        @POST("/api/v1/point/store")
        Call<Result<User>> insertVoucher(@Field("transaction_id") String transaction_id);

        @FormUrlEncoded
        @POST("/api/v1/update/check")
        Call<Result<Update>> checkDevice(@Field("uuid") String uuid, @Field("version_code") String  version_code, @Field("name") String  name, @Field("model") String  model, @Field("platform") String platform, @Field("platform_version") String platform_version, @Field("gcm") String gcm);



        @GET("member/gmb_katalog_member/{id_merchant}/{id_katalog}")
        Call<ResultAPI<Photo>> getPhoto(@Path("id_merchant") String id_merchant, @Path("id_katalog") String id_katalog);


    }

}

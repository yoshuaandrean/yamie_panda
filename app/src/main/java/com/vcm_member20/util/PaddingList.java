package com.vcm_member20.util;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Yoshua Andrean on 08/07/2016.
 */
public class PaddingList extends RecyclerView.ItemDecoration {
    private int space;
    private int tipe;

    public PaddingList(int space, int tipe) {
        this.space = space;
        this.tipe = tipe;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {

        if(tipe==1) {
            outRect.bottom = space;
        }
        else
        {
            if(parent.getChildLayoutPosition(view) == 0 || parent.getChildLayoutPosition(view) == 1){
                outRect.top = space;
            }

            if (parent.getChildLayoutPosition(view) % 2 == 0) {
                outRect.right = space/2;
                outRect.left = space;
            }
            else{
                outRect.left = space/2;
                outRect.right = space;
            }
            outRect.bottom = space;

        }
    }
}
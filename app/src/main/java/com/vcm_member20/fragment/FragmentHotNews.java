package com.vcm_member20.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vcm_member20.R;
import com.vcm_member20.adapter.ListHotNewsAdapter;
import com.vcm_member20.model.NewsDiskon;
import com.vcm_member20.model.Post;
import com.vcm_member20.model.ResultAPI;
import com.vcm_member20.model.Results;
import com.vcm_member20.rest.RestClient;
import com.vcm_member20.util.Constants;
import com.vcm_member20.util.OnLoadMoreListener;
import com.vcm_member20.util.PaddingList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Yoshua on 6/21/2015.
 */
public class FragmentHotNews extends Fragment {

    private int limit = 10;
    RecyclerView mRecyclerView;
    ListHotNewsAdapter mAdapter;

    SwipeRefreshLayout mSwipeRefreshLayout ;

    ArrayList<Post> listNews = new ArrayList<Post>();

    Call<Results<Post>> call;
    RestClient.GitApiInterface service;

    LinearLayout Progress;
    ImageView progressimage, notfound;

    public int flagmasihadadata = 0;

    public int flagkoneksi = 0;

    ArrayList<Post> listPertama;

    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_recycleview, container, false);

        Progress = (LinearLayout) v.findViewById(R.id.progress);
        progressimage = (ImageView) v.findViewById(R.id.progressimage);
        notfound = (ImageView) v.findViewById(R.id.notfound);
        notfound.setVisibility(View.GONE);

        // Calling the RecyclerView
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new PaddingList(70, 1));

        loadDataFromServer();

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimary,
                R.color.colorPrimary);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Refreshing data on server
                loadDataFromServer();
            }
        });

        return v;
    }

    public void loadDataFromServer()
    {
        flagmasihadadata = 0;
        listNews.clear();
        service = RestClient.getClient(getActivity());
        call = service.getNews(String.valueOf(0), String.valueOf(limit));

        call.enqueue(new Callback<Results<Post>>() {
            @Override
            public void onResponse(Call<Results<Post>> call, Response<Results<Post>> response) {

                flagkoneksi = 0;
                mSwipeRefreshLayout.setRefreshing(false);
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("Load News Pertama", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Results<Post> result = response.body();
                    Log.d("Load News Pertama aye", "response = " + new Gson().toJson(result));
                    listNews = result.getData();

                    listPertama = result.getData();

                    mAdapter = new ListHotNewsAdapter(v.getContext(),listNews, mRecyclerView, FragmentHotNews.this, getActivity());
                    mAdapter.setLoaded();
                    mRecyclerView.setAdapter(mAdapter);

                    mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {

                        if(listPertama.size()<limit) {

                        }
                        else
                        {
                            loadMoreDataFromServer();

                        }

                        }
                    });

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    Log.e("Load News Pertama", String.valueOf(response.raw().toString()));

                }
            }

            @Override
            public void onFailure(Call<Results<Post>> call, Throwable t) {
                listNews.clear();
                flagkoneksi = 1;
                mSwipeRefreshLayout.setRefreshing(false);
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "gagalall");
                Toast.makeText(v.getContext(),R.string.cekkoneksi, Toast.LENGTH_LONG).show();


            }
        });
    }

    public void deleteItem()
    {
        listNews.remove(listNews.size() - 1);
        mAdapter.deleteItem(listNews.size() - 1);
    }

    public void loadMoreDataFromServer()
    {
        if (flagmasihadadata == 0) {

            listNews.add(null);
            mAdapter.addItem(null, listNews.size()-1);

            int index = mAdapter.getItemCount() - 1;

            Log.d("indexny", String.valueOf(index));
            service = RestClient.getClient(getActivity());
            call = service.getNews(String.valueOf(index), String.valueOf(limit));

            call.enqueue(new Callback<Results<Post>>() {
                @Override
                public void onResponse(Call<Results<Post>> call, Response<Results<Post>> response) {
                    flagkoneksi = 0;

                    listNews.remove(listNews.size() - 1);
                    mAdapter.deleteItem(listNews.size() - 1);

                    Progress.setVisibility(View.GONE);
                    progressimage.setVisibility(View.GONE);
                    notfound.setVisibility(View.GONE);
                    Log.d("Load news Next", "Status Code = " + response.code());
                    if (response.isSuccessful()) {
                        // request successful (status code 200, 201)
                        Results<Post> result = response.body();
                        Log.d("Load news Next aye", "response = " + new Gson().toJson(result));
                        ArrayList<Post> listTemp;
                        listTemp = result.getData();
                        if (listTemp.size() < limit) {
                            flagmasihadadata = 1;
                        }
                        for (int i = 0; i < listTemp.size(); i++) {
                            listNews.add(listTemp.get(i));
                        }
                        mAdapter.setLoaded();

                    } else {
                        // response received but request not successful (like 400,401,403 etc)
                        //Handle errors
                    }
                }

                @Override
                public void onFailure(Call<Results<Post>> call, Throwable t) {
                    flagkoneksi = 1;
                    mSwipeRefreshLayout.setRefreshing(false);
                    Progress.setVisibility(View.GONE);
                    progressimage.setVisibility(View.GONE);
                    notfound.setVisibility(View.GONE);
                    Log.d("MainActivity", "gagalall");
                    Toast.makeText(v.getContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();

                    listNews.remove(listNews.size() - 1);
                    mAdapter.deleteItem(listNews.size() - 1);

                    listNews.add(null);
                    mAdapter.addItem(null, listNews.size() - 1);
                }
            });
        }
    }
}
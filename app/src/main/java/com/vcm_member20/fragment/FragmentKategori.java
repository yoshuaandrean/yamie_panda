package com.vcm_member20.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vcm_member20.R;
import com.vcm_member20.adapter.ListKategoriAdapter;
import com.vcm_member20.model.Kategori;
import com.vcm_member20.model.ResultAPI;
import com.vcm_member20.rest.RestClient;
import com.vcm_member20.util.Constants;
import com.vcm_member20.util.OnLoadMoreListener;
import com.vcm_member20.util.PaddingList;

import java.util.ArrayList;


/**
 * Created by Yoshua on 6/21/2015.
 */
public class FragmentKategori extends Fragment {

//    RecyclerView mRecyclerView;
//    GridLayoutManager mLayoutManager;
//    private ListKategoriAdapter mAdapter;
//
//    SwipeRefreshLayout mSwipeRefreshLayout ;
//
//    private ArrayList<Kategori> listKategori = new ArrayList<Kategori>();
//
//    Call<ResultAPI<Kategori>> call;
//    RestClient.GitApiInterface service;
//
//    LinearLayout Progress;
//    ImageView progressimage, notfound;
//
//    private int limit = 10;
//    int flagmasihadadata = 0;
//
//    public int flagkoneksi = 0;
//
//    ArrayList<Kategori> listPertama;
//
//    View v;
//
//    ArrayList<Kategori> listTemp;
//
//    int i;
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//
//        v = inflater.inflate(R.layout.fragment_recycleview, container, false);
//
//        Progress = (LinearLayout) v.findViewById(R.id.progress);
//        progressimage = (ImageView) v.findViewById(R.id.progressimage);
//        notfound = (ImageView) v.findViewById(R.id.notfound);
//        notfound.setVisibility(View.GONE);
//
//        // Calling the RecyclerView
//        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
//        mRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
//        mRecyclerView.setHasFixedSize(true);
//        mRecyclerView.addItemDecoration(new PaddingList(10, 0));
//
//        // The number of Columns
//        mLayoutManager = new GridLayoutManager(getActivity(), 2);
//
//        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
//            @Override
//            public int getSpanSize(int position) {
//
//                //Log.d("flagprog",String.valueOf(mAdapter.flagprog));
//                if(mAdapter.flagprog == 0) {
//
//                    mAdapter.setProgressOffSpan();
//                    return 2;
//                }
//                else {
//                    return 1;
//                }
//            }
//        });
//
//        mRecyclerView.setLayoutManager(mLayoutManager);
//
//        loadDataFromServer();
//
//        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
//
//        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                int topRowVerticalPosition =
//                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
//                mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
//
//            }
//
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//        });
//
//        mSwipeRefreshLayout.setColorSchemeResources(
//                R.color.colorPrimary,
//                R.color.colorPrimary,
//                R.color.colorPrimary);
//
//        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                //Refreshing data on server
//                loadDataFromServer();
//            }
//        });
//
//        return v;
//    }
//
//
//
//    public void loadDataFromServer()
//    {
//        flagmasihadadata = 0;
//        listKategori.clear();
//        service = RestClient.getClient();
//        call = service.getKategori(Constants.id_merchant, String.valueOf(0), String.valueOf(limit));
//
//        call.enqueue(new Callback<ResultAPI<Kategori>>() {
//            @Override
//            public void onResponse(Response<ResultAPI<Kategori>> response) {
//                flagkoneksi = 0;
//                mSwipeRefreshLayout.setRefreshing(false);
//                Progress.setVisibility(View.GONE);
//                progressimage.setVisibility(View.GONE);
//                notfound.setVisibility(View.GONE);
//                Log.d("Load Katalog Pertama", "Status Code = " + response.code());
//                if (response.isSuccess()) {
//                    // request successful (status code 200, 201)
//
//                    ResultAPI<Kategori> result = response.body();
//                    Log.d("Load Katalog Pertama", "response = " + new Gson().toJson(result));
//                    listKategori = result.getIsi();
//
//                    listPertama = result.getIsi();
//
//                    mAdapter = new ListKategoriAdapter(v.getContext(), listKategori, mRecyclerView, FragmentKategori.this, getActivity());
//                    mAdapter.setLoaded();
//                    mRecyclerView.setAdapter(mAdapter);
//
//                    mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
//                        @Override
//                        public void onLoadMore() {
//
//                            if(listPertama.size()<limit) {
//
//                            }
//                            else
//                            {
//                                loadMoreDataFromServer();
//
//                            }
//
//                        }
//                    });
//
//                } else {
//                    // response received but request not successful (like 400,401,403 etc)
//                    //Handle errors
//
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//
//                flagkoneksi = 1;
//                mSwipeRefreshLayout.setRefreshing(false);
//                Progress.setVisibility(View.GONE);
//                progressimage.setVisibility(View.GONE);
//                notfound.setVisibility(View.GONE);
//                Log.d("MainActivity", "gagalall");
//                Toast.makeText(v.getContext(),R.string.cekkoneksi, Toast.LENGTH_LONG).show();
//
////                listKategori = db.getDataKatalog();
////                mAdapter = new ListKatalogAdapter(v.getContext(), listKategori, mRecyclerView, FragmentKategori.this, getActivity());
////                mAdapter.setLoaded();
////                mRecyclerView.setAdapter(mAdapter);
//
//
//            }
//        });
//    }
//
//    public void loadMoreDataFromServer()
//    {
//
//        Log.d("flagmasihadadata", String.valueOf(flagmasihadadata));
//
//        if (flagmasihadadata == 0) {
//
//            listKategori.add(null);
//
//            android.os.Handler mHandler = getActivity().getWindow().getDecorView().getHandler();
//            mHandler.post(new Runnable() {
//                public void run() {
//                    //change adapter contents
//                    mAdapter.addItem(null, listKategori.size() - 1);
//
//                }
//            });
//
//            int index = mAdapter.getItemCount() - 1;
//
//            Log.d("indexny", String.valueOf(index));
//            service = RestClient.getClient();
//            call = service.getKategori(Constants.id_merchant, String.valueOf(index), String.valueOf(limit));
//
//            call.enqueue(new Callback<ResultAPI<Kategori>>() {
//
//                @Override
//                public void onResponse(Response<ResultAPI<Kategori>> response) {
//                    flagkoneksi = 0;
//                    listKategori.remove(listKategori.size() - 1);
//                    mAdapter.deleteItem(listKategori.size());
//
//                    Progress.setVisibility(View.GONE);
//                    progressimage.setVisibility(View.GONE);
//                    notfound.setVisibility(View.GONE);
//                    Log.d("Load Katalog Next", "Status Code = " + response.code());
//                    if (response.isSuccess()) {
//                        // request successful (status code 200, 201)
//                        ResultAPI<Kategori> result = response.body();
//                        Log.d("Load Katalog Next aye", "response = " + new Gson().toJson(result));
//
//                        listTemp = result.getIsi();
//                        if (listTemp.size() < limit) {
//                            flagmasihadadata = 1;
//                        }
//                        for (i = 0; i < listTemp.size(); i++) {
//                            listKategori.add(listTemp.get(i));
//                            mAdapter.addItem(listTemp.get(i), listKategori.size() - 1);
//
//                        }
//                        mAdapter.setLoaded();
//
//                    } else {
//                        // response received but request not successful (like 400,401,403 etc)
//                        //Handle errors
//                    }
//                }
//
//                @Override
//                public void onFailure(Throwable t) {
//                    flagkoneksi = 1;
//                    mSwipeRefreshLayout.setRefreshing(false);
//                    Progress.setVisibility(View.GONE);
//                    progressimage.setVisibility(View.GONE);
//                    notfound.setVisibility(View.GONE);
//                    Log.d("next gagal", "gagalall");
//                    Toast.makeText(v.getContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();
//
//                    listKategori.remove(listKategori.size() - 1);
//                    mAdapter.deleteItem(listKategori.size());
//
//                    mAdapter.setProgressOnSpan();
//
//                    listKategori.add(null);
//                    android.os.Handler mHandler = getActivity().getWindow().getDecorView().getHandler();
//                    mHandler.post(new Runnable() {
//                        public void run() {
//                            //change adapter contents
//
//
//                            mAdapter.addItem(null, listKategori.size() - 1);
//
//                        }
//                    });
//                    mAdapter.setProgressOffSpan();
//
//                }
//            });
//        }
//    }
//
//    public void deleteItem()
//    {
//        listKategori.remove(listKategori.size() - 1);
//        mAdapter.deleteItem(listKategori.size());
//    }
//
////    public void getKatalogCari(String cari)
////    {
////        service = RestClient.getClient();
////        call = service.getKatalogSearch(Constants.id_merchant, cari);
////
////        call.enqueue(new Callback<ResultAPI<Katalog>>() {
////            @Override
////            public void onResponse(Response<ResultAPI<Katalog>> response) {
////                Progress.setVisibility(View.GONE);
////                progressimage.setVisibility(View.GONE);
////                notfound.setVisibility(View.GONE);
////                Log.d("MainActivity", "Status Code = " + response.code());
////                if (response.isSuccess()) {
////                    // request successful (status code 200, 201)
////                    ResultAPI<Katalog> result = response.body();
////                    Log.d("MainActivity", "response = " + new Gson().toJson(result));
////                    listKatalog = result.getIsi();
////                    mAdapter = new ListKatalogAdapter(v.getContext(), listKatalog, mRecyclerView, FragmentKategori.this, getActivity());
////                    mRecyclerView.setAdapter(mAdapter);
////
////                } else {
////                    // response received but request not successful (like 400,401,403 etc)
////                    //Handle errors
////
////                }
////            }
////
////            @Override
////            public void onFailure(Throwable t) {
////                Progress.setVisibility(View.GONE);
////                progressimage.setVisibility(View.GONE);
////                notfound.setVisibility(View.GONE);
////                Log.d("MainActivity", "gagalall");
////                Toast.makeText(v.getContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();
////            }
////        });
////
////
////    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setHasOptionsMenu(true);
//    }

}

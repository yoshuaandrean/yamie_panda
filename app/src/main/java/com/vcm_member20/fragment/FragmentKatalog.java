package com.vcm_member20.fragment;

import android.app.SearchManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vcm_member20.R;
import com.vcm_member20.activity.KatalogActivity;
import com.vcm_member20.adapter.ListKatalogAdapter;
import com.vcm_member20.model.Katalog;
import com.vcm_member20.model.Post;
import com.vcm_member20.model.ResultAPI;
import com.vcm_member20.model.Results;
import com.vcm_member20.rest.RestClient;
import com.vcm_member20.util.Constants;
import com.vcm_member20.util.OnLoadMoreListener;
import com.vcm_member20.util.PaddingList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Yoshua on 6/21/2015.
 */
public class FragmentKatalog extends Fragment implements SearchView.OnQueryTextListener {

    RecyclerView mRecyclerView;
    StaggeredGridLayoutManager mLayoutManager;
    private ListKatalogAdapter mAdapter;

    SwipeRefreshLayout mSwipeRefreshLayout ;

    private ArrayList<Post> listKatalog = new ArrayList<Post>();

    Call<Results<Post>> call;
    RestClient.GitApiInterface service;

    LinearLayout Progress;
    ImageView progressimage, notfound;

    private int limit = 10;
    int flagmasihadadata = 0;

    private SearchView mSearchView;

    public int flagkoneksi = 0;

    ArrayList<Post> listPertama = new ArrayList<Post>();

    View v;

    ArrayList<Post> listTemp = new ArrayList<Post>();

    int i;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_recycleview, container, false);

        Progress = (LinearLayout) v.findViewById(R.id.progress);
        progressimage = (ImageView) v.findViewById(R.id.progressimage);
        notfound = (ImageView) v.findViewById(R.id.notfound);
        notfound.setVisibility(View.GONE);

        // Calling the RecyclerView
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new PaddingList(10, 0));

        // The number of Columns
        mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);


        mRecyclerView.setLayoutManager(mLayoutManager);

        loadDataFromServer();

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimary,
                R.color.colorPrimary);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Refreshing data on server
                loadDataFromServer();
                KatalogActivity.setFragment();
            }
        });

        return v;
    }



    public void loadDataFromServer()
    {
        flagmasihadadata = 0;
        listKatalog.clear();
        listTemp.clear();
        listPertama.clear();
        service = RestClient.getClient(getActivity());
        call = service.getKatalog(String.valueOf(0),String.valueOf(limit));

        call.enqueue(new Callback<Results<Post>>() {
            @Override
            public void onResponse(Call<Results<Post>> call, Response<Results<Post>> response) {
                flagkoneksi = 0;
                mSwipeRefreshLayout.setRefreshing(false);
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("Load Katalog Pertama", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)

                    Results<Post> result = response.body();
                    Log.d("Load Katalog Pertama", "response = " + new Gson().toJson(result));
                    listKatalog = result.getData();
                    listPertama = result.getData();

                    mAdapter = new ListKatalogAdapter(v.getContext(), listKatalog, mRecyclerView, FragmentKatalog.this, getActivity());
                    mAdapter.setLoaded();
                    mRecyclerView.setAdapter(mAdapter);

                    mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {

                            if(listPertama.size()<limit) {

                            }
                            else
                            {
                                loadMoreDataFromServer();

                            }

                        }
                    });

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors

                }
            }

            @Override
            public void onFailure(Call<Results<Post>> call, Throwable t) {

                flagkoneksi = 1;
                mSwipeRefreshLayout.setRefreshing(false);
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "gagalall");
                Toast.makeText(v.getContext(),R.string.cekkoneksi,Toast.LENGTH_LONG).show();

            }
        });
    }

    public void loadMoreDataFromServer()
    {

        Log.d("flagmasihadadata", String.valueOf(flagmasihadadata));

        if (flagmasihadadata == 0) {

            listKatalog.add(null);
            Log.e("ew", String.valueOf(listKatalog.size()));
            android.os.Handler mHandler = getActivity().getWindow().getDecorView().getHandler();
            mHandler.post(new Runnable() {
                public void run() {
                    //change adapter contents
                    mAdapter.addItem(null, listKatalog.size() - 1);
                    mAdapter.notifyDataSetChanged();
                }
            });

            int index = mAdapter.getItemCount() - 1;

            Log.d("indexny", String.valueOf(index));
            service = RestClient.getClient(getActivity());
            call = service.getKatalog(String.valueOf(index),String.valueOf(limit));

            call.enqueue(new Callback<Results<Post>>() {
                @Override
                public void onResponse(Call<Results<Post>> call, Response<Results<Post>> response) {
                    flagkoneksi = 0;
                    listKatalog.remove(listKatalog.size() - 1);
                    mAdapter.deleteItem(listKatalog.size()-1);

                    Progress.setVisibility(View.GONE);
                    progressimage.setVisibility(View.GONE);
                    notfound.setVisibility(View.GONE);
                    Log.d("Load Katalog Next", "Status Code = " + response.code());
                    if (response.isSuccessful()) {
                        // request successful (status code 200, 201)
                        Results<Post> result = response.body();
                        Log.d("Load Katalog Next aye", "response = " + new Gson().toJson(result));

                        listTemp = result.getData();
                        Log.e("listTemp", String.valueOf(listTemp.size()));
                        Log.e("listKatalog", String.valueOf(listKatalog.size()));
                        if (listTemp.size() < limit) {
                            flagmasihadadata = 1;
                        }
                        for (i = 0; i < listTemp.size(); i++) {
                            listKatalog.add(listTemp.get(i));
                        }
                        mAdapter.setLoaded();

                    } else {
                        // response received but request not successful (like 400,401,403 etc)
                        //Handle errors
                    }
                }

                @Override
                public void onFailure(Call<Results<Post>> call, Throwable t) {
                    flagkoneksi = 1;
                    mSwipeRefreshLayout.setRefreshing(false);
                    Progress.setVisibility(View.GONE);
                    progressimage.setVisibility(View.GONE);
                    notfound.setVisibility(View.GONE);
                    Log.d("next gagal", "gagalall");
                    Toast.makeText(v.getContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();

                    listKatalog.remove(listKatalog.size() - 1);
                    mAdapter.deleteItem(listKatalog.size() - 1);


                    listKatalog.add(null);
                    android.os.Handler mHandler = getActivity().getWindow().getDecorView().getHandler();
                    mHandler.post(new Runnable() {
                        public void run() {
                            //change adapter contents
                            mAdapter.addItem(null, listKatalog.size() - 1);
                        }
                    });

                }
            });
        }
    }

    public void deleteItem()
    {
        listKatalog.remove(listKatalog.size() - 1);
        mAdapter.deleteItem(listKatalog.size() - 1);
    }

    public void getKatalogCari(String cari)
    {
        listKatalog.clear();
        listPertama.clear();
        service = RestClient.getClient(getActivity());
        String with = "asset";
        String search = "title:"+cari;
        String searchFields = "title:=";
        call = service.getKatalogCari(search, searchFields, with);

        call.enqueue(new Callback<Results<Post>>() {
            @Override
            public void onResponse(Call<Results<Post>> call, Response<Results<Post>> response) {
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Results<Post> result = response.body();
                    Log.d("MainActivity", "response = " + new Gson().toJson(result));
                    listKatalog = result.getData();
                    mAdapter = new ListKatalogAdapter(v.getContext(), listKatalog, mRecyclerView, FragmentKatalog.this, getActivity());
                    mRecyclerView.setAdapter(mAdapter);

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors

                }
            }

            @Override
            public void onFailure(Call<Results<Post>> call, Throwable t) {
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "gagalall");
                Toast.makeText(v.getContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_katalog, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) searchItem.getActionView();
        setupSearchView(searchItem);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setupSearchView(MenuItem searchItem) {

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null) {

            mSearchView.setQueryHint("Cari Produk");
            View searchPlate = mSearchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
            searchPlate.setBackgroundResource(R.drawable.abc_textfield_search_material);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }

        mSearchView.setOnQueryTextListener(this);
    }

    public boolean onQueryTextChange(String query) {

        return false;
    }

    public boolean onQueryTextSubmit(String query) {
        String keyword = query;

        getKatalogCari(keyword);

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mSearchView.getWindowToken(), 0);
        return false;
    }

}

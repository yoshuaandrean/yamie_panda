package com.vcm_member20.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vcm_member20.R;
import com.vcm_member20.model.Result;
import com.vcm_member20.model.User;
import com.vcm_member20.rest.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EditPasswordActivity extends AppCompatActivity {

    private EditText edpassword, edpasswordbaru, edpasswordbaruulang;

    private Button btnsimpan;

    private ProgressDialog pDialog;

    int id,client_id;
    String name,email,birthday,religion,phone,address,photo,created_at,updated_at, status;

    private SharedPreferences sharedPreferences;

    public void loadPreferences() {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            id = sharedPreferences.getInt("id", 0);
            client_id = sharedPreferences.getInt("client_id", 0);
            name = sharedPreferences.getString("name", "");
            email = sharedPreferences.getString("email", "");
            birthday = sharedPreferences.getString("birthday", "");
            religion = sharedPreferences.getString("religion", "");
            phone = sharedPreferences.getString("phone", "");
            address = sharedPreferences.getString("address", "");
            photo = sharedPreferences.getString("photo", "");
            status = sharedPreferences.getString("status", "");
            created_at = sharedPreferences.getString("created_at", "");
            updated_at = sharedPreferences.getString("updated_at", "");
        } else {

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_password);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        loadPreferences();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        edpassword = (EditText) findViewById(R.id.passwordlama);
        edpasswordbaru = (EditText) findViewById(R.id.passwordbaru);
        edpasswordbaruulang = (EditText) findViewById(R.id.passwordbaruulang);

        btnsimpan = (Button) findViewById(R.id.btnsimpan);

        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPassword();
            }
        });

    }


    public void editPassword()
    {
        if(edpasswordbaru.getText().toString().equalsIgnoreCase(edpasswordbaruulang.getText().toString()))
        {
            pDialog = ProgressDialog.show(EditPasswordActivity.this,
                    "Mengedit Password Anda",
                    "Tunggu Sebentar!");

            RestClient.GitApiInterface service = RestClient.getClient(this);
            //File a = new File();
            Call<Result<User>> call = service.updatePassword(edpassword.getText().toString(), edpasswordbaru.getText().toString());

            call.enqueue(new Callback<Result<User>>() {
                @Override
                public void onResponse(Call<Result<User>> call, Response<Result<User>> response) {
                    pDialog.dismiss();
                    Log.d("Update Password", "Status Code = " + response.code());
                    if (response.isSuccessful()) {
                        Result<User> result = response.body();
                        Log.d("Update Password", "response = " + new Gson().toJson(result));
                        if(result.getSuccess()){
                            Toast.makeText(getApplicationContext(), result.getMessage(), Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            Log.e("Update Password", response.body().getMessage());
                            pDialog.dismiss();
                            Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                        }

                    } else {
                        Log.e("Update Password", String.valueOf(response.raw().toString()));
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Result<User>> call,Throwable t) {
                    pDialog.dismiss();
                    Log.e("on Failure", t.toString());
                    Log.d("gagal", "agagagagal");
                    Toast.makeText(getApplicationContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();
                }
            });
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Password baru anda tidak sama", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                EditPasswordActivity.this.finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

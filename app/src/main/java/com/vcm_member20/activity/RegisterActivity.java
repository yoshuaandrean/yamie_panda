package com.vcm_member20.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vcm_member20.R;
import com.vcm_member20.model.Result;
import com.vcm_member20.model.Token;
import com.vcm_member20.model.User;
import com.vcm_member20.rest.RestClient;
import com.vcm_member20.util.Constants;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    protected static final int DATE_DIALOG_ID = 0;

    private ImageButton btndaftar;

    private TextView cagree;

    public static Activity ActRegister;
    private ProgressDialog pDialog;

    private SharedPreferences sharedPreferences;

    private EditText ednama, edemail, edtelp, edalamat, edpassword, edlahir;

    private BetterSpinner spagama;

    private CheckBox cek;

    String nama, telp, alamat, email, password, tgl_lahir, agama, tanggal;

    private void savePreferences(boolean islogin,
                                 int id,
                                 int client_id,
                                 String type,
                                 String name,
                                 String email,
                                 String poin,
                                 String birthday,
                                 String religion,
                                 String phone,
                                 String address,
                                 String photo,
                                 String status,
                                 String access,
                                 String created_at,
                                 String updated_at) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("islogin", islogin);
        editor.putInt("id", id);
        editor.putInt("client_id", client_id);
        editor.putString("type", type);
        editor.putString("name", name);
        editor.putString("email", email);
        editor.putString("poin", poin);
        editor.putString("birthday", birthday);
        editor.putString("religion", religion);
        editor.putString("phone", phone);
        editor.putString("address", address);
        editor.putString("photo", photo);
        editor.putString("status", status);
        editor.putString("access", access);
        editor.putString("created_at", created_at);
        editor.putString("updated_at", updated_at);
        editor.commit();
    }

    private void saveLogin(boolean islogin) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("islogin", islogin);
        editor.commit();
    }

    private void saveToken(String user_token) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user_token", user_token);
        editor.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ActRegister = this;

        btndaftar = (ImageButton) findViewById(R.id.btndaftar);

        cek = (CheckBox) findViewById(R.id.checkBox);
        cek.setChecked(true);

        cagree = (TextView) findViewById(R.id.cagree);

        cagree.setClickable(true);
        cagree.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "Saya setuju dengan <a style='color:#4EA30D' href=' http://kartuvirtual.com'>persetujuan</a> dan <a href=' http://kartuvirtual.com'>kebijakan</a> dari pengembang dan pengembang aplikasi";
        cagree.setText(Html.fromHtml(text));

        ednama = (EditText) findViewById(R.id.nama);
        edalamat = (EditText) findViewById(R.id.alamat);
        edemail = (EditText) findViewById(R.id.email);
        edtelp = (EditText) findViewById(R.id.telp);
        edpassword = (EditText) findViewById(R.id.password);
        edlahir = (EditText) findViewById(R.id.lahir);
        spagama = (BetterSpinner) findViewById(R.id.agama);
        String[] list = getResources().getStringArray(R.array.spinneragama);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, list);

        spagama.setAdapter(adapter);

        edlahir.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (v == edlahir)
                    showDialog(DATE_DIALOG_ID);
                return false;
            }
        });

        btndaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cekRegister();

            }
        });
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private void cekRegister()
    {
        boolean cancel = true;

        nama = ednama.getText().toString();
        telp = edtelp.getText().toString();
        alamat = edalamat.getText().toString();
        email = edemail.getText().toString();
        password = edpassword.getText().toString();
        tgl_lahir = tanggal;
        agama = spagama.getText().toString();

        Log.d("agama", agama);

        if (email.equalsIgnoreCase("")||alamat.equalsIgnoreCase("")||telp.equalsIgnoreCase("")||nama.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(),"Data tidak boleh ada yang kosong", Toast.LENGTH_LONG).show();
            cancel = false;
        } else if (!isEmailValid(email)) {
            Toast.makeText(getApplicationContext(),R.string.invalidemail, Toast.LENGTH_LONG).show();
            cancel = false;
        }

        if (cancel) {

            if(cek.isChecked()) {
                pDialog = ProgressDialog.show(RegisterActivity.this,
                        "Mendaftar",
                        "Tunggu Sebentar!");

                register();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Anda harus menyetujui persetujuan yang kami berikan", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void register()
    {
        RestClient.GitApiInterface service = RestClient.getClient(this);
        Call<Result<User>> call = service.register(Constants.client_id, email, password, nama, telp, alamat, tgl_lahir, agama);
        call.enqueue(new Callback<Result<User>>() {
            @Override
            public void onResponse(Call<Result<User>> call, Response<Result<User>> response) {
                Log.d("Register", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    Result<User> result = response.body();
                    Log.d("Register", "response = " + new Gson().toJson(result));
                    if(result.getSuccess())
                    {
                        getToken(email, password);
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Email anda sudah terdaftar", Toast.LENGTH_LONG).show();
                    }

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    Log.e("Register", String.valueOf(response.raw().toString()));
                    pDialog.dismiss();

                }
            }

            @Override
            public void onFailure(Call<Result<User>> call, Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Log.d("gagal", "agagagagal");
                Toast.makeText(getApplicationContext(),R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getToken(String email, String password)
    {
        RestClient.GitApiInterface service = RestClient.getClient(this);
        Call<Result<Token>> call = service.getToken(Constants.client_id, email,password);
        call.enqueue(new Callback<Result<Token>>() {
            @Override
            public void onResponse(Call<Result<Token>> call, Response<Result<Token>> response) {
                Log.d("Login", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Result<Token> result = response.body();
                    Log.d("Login", "response = " + new Gson().toJson(result));
                    if(result.getSuccess())
                    {
                        saveToken(result.getData().getToken());
                        Log.d("Login", "Token: " + response.body().getData().getToken());
                        getProfil();
                    }
                    else {
                        Log.e("Login", response.body().getMessage());
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Log.e("Login", String.valueOf(response.raw().toString()));
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Anda belum terverifikasi",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Result<Token>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Toast.makeText(getApplicationContext(),R.string.cekkoneksi,Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getProfil()
    {
        RestClient.GitApiInterface service = RestClient.getClient(this);
        Call<Result<User>> call = service.getProfil();

        call.enqueue(new Callback<Result<User>>() {
            @Override
            public void onResponse(Call<Result<User>> call,Response<Result<User>> response) {
                Log.d("Get Profil", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    Result<User> result = response.body();
                    Log.d("Get Profil", "response = " + new Gson().toJson(result));
                    if(result.getSuccess())
                    {
                        saveLogin(true);

                        savePreferences(true,
                                result.getData().getId(),
                                result.getData().getClient_id(),
                                result.getData().getType(),
                                result.getData().getName(),
                                result.getData().getEmail(),
                                result.getData().getPoint(),
                                result.getData().getBirthday(),
                                result.getData().getReligion(),
                                result.getData().getPhone(),
                                result.getData().getAddress(),
                                result.getData().getPhoto(),
                                result.getData().getStatus(),
                                result.getData().getAccess(),
                                result.getData().getCreated_at(),
                                result.getData().getUpdated_at());


                        pDialog.dismiss();
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        LoginActivity.ActLogin.finish();
                        startActivity(i);
                        finish();

                    }
                    else {
                        Log.e("Get Profil", response.body().getMessage());
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Log.e("Get Profil", String.valueOf(response.raw().toString()));
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Tidak Dapat",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Result<User>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Toast.makeText(getApplicationContext(),R.string.cekkoneksi,Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        Calendar c = Calendar.getInstance();
        int cyear = c.get(Calendar.YEAR);
        int cmonth = c.get(Calendar.MONTH);
        int cday = c.get(Calendar.DAY_OF_MONTH);

        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, mDateSetListener, cyear, cmonth, cday);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            String date_selected = String.valueOf(year)+"-"
                    + String.valueOf(monthOfYear + 1)+ "-"
                    + String.valueOf(dayOfMonth);

            String bulan = "";
            if((monthOfYear+1)==1)
            {
                bulan = "Januari";
            }
            if((monthOfYear+1)==2)
            {
                bulan = "Februari";
            }
            if((monthOfYear+1)==3)
            {
                bulan = "Maret";
            }
            if((monthOfYear+1)==4)
            {
                bulan = "April";
            }
            if((monthOfYear+1)==5)
            {
                bulan = "Mei";
            }
            if((monthOfYear+1)==6)
            {
                bulan = "Juni";
            }
            if((monthOfYear+1)==7)
            {
                bulan = "Juli";
            }
            if((monthOfYear+1)==8)
            {
                bulan = "Agustus";
            }
            if((monthOfYear+1)==9)
            {
                bulan = "September";
            }
            if((monthOfYear+1)==10)
            {
                bulan = "Oktober";
            }
            if((monthOfYear+1)==11)
            {
                bulan = "November";
            }
            if((monthOfYear+1)==12)
            {
                bulan = "Desember";
            }

            tanggal = date_selected;
            edlahir.setText(String.valueOf(dayOfMonth)+" "+bulan+" "+ String.valueOf(year));
        }
    };

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

}

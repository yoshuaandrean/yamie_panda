package com.vcm_member20.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderViewKenBurn;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.gson.Gson;
import com.vcm_member20.R;
import com.vcm_member20.model.Results;
import com.vcm_member20.model.Share;
import com.vcm_member20.rest.RestClient;
import com.vcm_member20.util.TimeFormater;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetilHotNewsActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{
    private Intent shareIntent;

    private TextView txtdeskripsi, txtnama, txttanggal;

    private String bundleid, bundlejudul, bundleisi, bundlelengkap, bundlephoto, bundlemerchant, share = "";

    private SliderLayout mDemoSlider;

    private CollapsingToolbarLayout collapsingToolbarLayout;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_hot_news);

        mDemoSlider = (SliderLayout)findViewById(R.id.slider);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        Bundle b = getIntent().getExtras();
        bundleid = b.getString("idnews");
        bundlejudul = b.getString("judul");
        bundleisi = b.getString("isi");
        bundlephoto = b.getString("photo").replace("\\", "/");
        bundlemerchant = b.getString("merchant");
        bundlelengkap = b.getString("tgl_lengkap");

        getSupportActionBar().setTitle(bundlejudul);

        Typeface bold = Typeface.createFromAsset(getApplicationContext().getAssets(),  "Quicksand-Bold.otf");

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(bundlejudul);
        collapsingToolbarLayout.setCollapsedTitleTypeface(bold);
        collapsingToolbarLayout.setExpandedTitleTypeface(bold);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));


        txtdeskripsi = (TextView) findViewById(R.id.deskripsi);
        txtnama = (TextView) findViewById(R.id.nama);
        txttanggal = (TextView) findViewById(R.id.tanggal);
        txtnama.setText(bundlejudul);
        txtnama.setTypeface(bold);
        txtdeskripsi.setText(bundleisi);
        txtdeskripsi.setTypeface(bold);
        txttanggal.setText(new TimeFormater().formattedDateFromString("yyyy-MM-dd", "dd MMMM yyyy", bundlelengkap));
        txttanggal.setTypeface(bold);

        TextSliderViewKenBurn textSliderView = new TextSliderViewKenBurn(this);
        textSliderView
                .image(bundlephoto)
                .setScaleType(BaseSliderView.ScaleType.Fit)
                .setOnSliderClickListener(this);

        Log.d("photo", bundlephoto);
        textSliderView.bundle(new Bundle());
        textSliderView.getBundle()
                .putString("photo",bundlephoto);

        mDemoSlider.addSlider(textSliderView);
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setDuration(60000);
        mDemoSlider.addOnPageChangeListener(this);
        mDemoSlider.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));

        getShare();

    }

    public void setupShareIntent() {
        Uri bmpUri = getLocalBitmapUri(TextSliderViewKenBurn.target);
        shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, bundlejudul);
        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, "Share ke media sosial . ."));
    }

    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }

        Uri bmpUri = null;
        try {
            File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public void getShare()
    {
        pDialog = ProgressDialog.show(this,
                "",
                "Tunggu Sebentar!");
        RestClient.GitApiInterface service = RestClient.getClient(this);
        Call<Results<Share>> call = service.getShare();

        call.enqueue(new Callback<Results<Share>>() {
            @Override
            public void onResponse(Call<Results<Share>> call, Response<Results<Share>> response) {
                pDialog.dismiss();
                if (response.isSuccessful()) {
                    Results<Share> result = response.body();
                    Log.d("Get Share", "response = " + new Gson().toJson(result));
                    if(result.getData().size()>0){
                        share = result.getData().get(0).getName();
                    }
                } else {
                    Log.e("Get Share", String.valueOf(response.raw().toString()));
                    Toast.makeText(getApplicationContext(),"Tidak Dapat", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Results<Share>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Toast.makeText(getApplicationContext(),R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void setDialogShare(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inf = LayoutInflater.from(this);
        View subView = inf.inflate(R.layout.dialog_share, null);
        builder.setView(subView);

        AlertDialog mDialog = builder.create();
        //mDialog.setCancelable(false);

        LinearLayout llfacebook = (LinearLayout) subView.findViewById(R.id.llFacebook);
        LinearLayout llLine = (LinearLayout) subView.findViewById(R.id.llLine);
        LinearLayout llInstagram = (LinearLayout) subView.findViewById(R.id.llInstagram);

        llfacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri bmpUri = getLocalBitmapUri(TextSliderViewKenBurn.target);

                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("image/*");
                shareIntent.putExtra(Intent.EXTRA_STREAM,bmpUri);
                shareIntent.putExtra(Intent.EXTRA_TEXT,bundlejudul);
                shareIntent.setPackage("com.facebook.katana");
                startActivity(Intent.createChooser(shareIntent, "Share ke Facebook"));
            }
        });

        llLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri bmpUri = getLocalBitmapUri(TextSliderViewKenBurn.target);

                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("image/*");
                shareIntent.putExtra(Intent.EXTRA_STREAM,bmpUri);
                shareIntent.putExtra(Intent.EXTRA_TEXT,bundlejudul);
                shareIntent.setPackage("jp.naver.line.android");
                startActivity(Intent.createChooser(shareIntent, "Share ke Line"));
            }
        });

        llInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri bmpUri = getLocalBitmapUri(TextSliderViewKenBurn.target);

                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("image/*");
                shareIntent.putExtra(Intent.EXTRA_STREAM,bmpUri);
                shareIntent.putExtra(Intent.EXTRA_TEXT,bundlejudul);
                shareIntent.setPackage("com.instagram.android");
                startActivity(Intent.createChooser(shareIntent, "Share ke Instagram"));
            }
        });

        llfacebook.setVisibility(View.GONE);
        llLine.setVisibility(View.GONE);
        llInstagram.setVisibility(View.GONE);

        if(share.equalsIgnoreCase("Facebook")){
            llfacebook.setVisibility(View.VISIBLE);
        }
        if(share.equalsIgnoreCase("Line")){
            llLine.setVisibility(View.VISIBLE);
        }
        if(share.equalsIgnoreCase("Instagram")){
            llInstagram.setVisibility(View.VISIBLE);
        }

        mDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                finish();

                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                return true;

            case R.id.menu_item_share:

                setDialogShare();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}

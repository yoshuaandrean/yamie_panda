package com.vcm_member20.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.vcm_member20.R;
import com.vcm_member20.gcm.QuickstartPreferences;
import com.vcm_member20.gcm.RegistrationIntentService;
import com.vcm_member20.model.Post;
import com.vcm_member20.model.Result;
import com.vcm_member20.model.Results;
import com.vcm_member20.model.Update;
import com.vcm_member20.model.User;
import com.vcm_member20.rest.RestClient;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{

    private ArrayList<Post> mList = new ArrayList<>();
    private SliderLayout mDemoSlider;
    private Call<Results<Post>> call;
    private RestClient.GitApiInterface service;

    private LinearLayout katalog, hotnews, order, chatting, profil, pengaturan;

    private TextView txtkatalog, txthotnews, txtorder, txtchatting, txtprofil, txtpengaturan, txtmerchant;

    private ProgressDialog pDialog;

    private String telp, email;

    private AlertDialog dialog;

    public static Activity act;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private SharedPreferences sharedPreferences;

    private void savePreferences(boolean islogin,
                                 int id,
                                 int client_id,
                                 String type,
                                 String name,
                                 String email,
                                 String poin,
                                 String birthday,
                                 String religion,
                                 String phone,
                                 String address,
                                 String photo,
                                 String status,
                                 String access,
                                 String created_at,
                                 String updated_at) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("islogin", islogin);
        editor.putInt("id", id);
        editor.putInt("client_id", client_id);
        editor.putString("type", type);
        editor.putString("name", name);
        editor.putString("email", email);
        editor.putString("poin", poin);
        editor.putString("birthday", birthday);
        editor.putString("religion", religion);
        editor.putString("phone", phone);
        editor.putString("address", address);
        editor.putString("photo", photo);
        editor.putString("status", status);
        editor.putString("access", access);
        editor.putString("created_at", created_at);
        editor.putString("updated_at", updated_at);
        editor.commit();
    }


    private void saveToken(String token) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token", token);
        editor.commit();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        act = this;

        mDemoSlider = (SliderLayout)findViewById(R.id.slider);

        Typeface bold = Typeface.createFromAsset(getApplicationContext().getAssets(),  "Quicksand-Bold.otf");
        txtkatalog = (TextView) findViewById(R.id.txtkatalog);
        txtkatalog.setTypeface(bold);
        txthotnews = (TextView) findViewById(R.id.txthotnews);
        txthotnews.setTypeface(bold);
        txtorder = (TextView) findViewById(R.id.txtorder);
        txtorder.setTypeface(bold);
        txtchatting = (TextView) findViewById(R.id.txtchatting);
        txtchatting.setTypeface(bold);
        txtprofil = (TextView) findViewById(R.id.txtprofil);
        txtprofil.setTypeface(bold);
        txtpengaturan = (TextView) findViewById(R.id.txtpengaturan);
        txtpengaturan.setTypeface(bold);

        txtmerchant = (TextView) findViewById(R.id.txtmerchant);
        txtmerchant.setTypeface(bold);
        txtmerchant.setText(getResources().getString(R.string.app_name));

        katalog = (LinearLayout) findViewById(R.id.katalog);
        katalog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), KatalogActivity.class);
                startActivity(i);
            }
        });

        hotnews = (LinearLayout) findViewById(R.id.hotnews);
        hotnews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), HotNewsActivity.class);
                startActivity(i);
            }
        });

        profil = (LinearLayout) findViewById(R.id.profil);
        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), TentangActivity.class);
                startActivity(i);
            }
        });

        pengaturan = (LinearLayout) findViewById(R.id.pengaturan);
        pengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), PengaturanActivity.class);
                startActivity(i);
            }
        });

        order = (LinearLayout) findViewById(R.id.order);
        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), PromoActivity.class);
                startActivity(i);
            }
        });

        chatting = (LinearLayout) findViewById(R.id.chatting);
        chatting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               chatMethod();
            }
        });

        if (checkPlayServices()) {
            //Jika iya, maka akan dipanggil Class RegistrationIntentService.java yang akan meminta Token dari server GCM
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                //Memanggil sharedPreferences yang memiliki informasi apakah token sudah didapatkan atau belum
                //dengan menggunakan getBoolean
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                String token = sharedPreferences.getString(QuickstartPreferences.REGISTRATION_TOKEN,"");

                if (sentToken) {
                    //Jika sentToken bernilai true, maka Token yang sudah didapatkan
                    //akan ditampilkan pada txtInfo dan juga Toast

                    checkDevice(token);
                    saveToken(token);

                } else {
                    Toast.makeText(getApplicationContext(), "Token gagal dibuat.. silahkan cek koneksi dan coba lagi",Toast.LENGTH_LONG).show();
                }
            }
        };

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA},
                    201);
        }
        if ( ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.CALL_PHONE},
                    1);
        }

        loadDataFromServer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getProfil();
    }

    public void  call(){
        if ( ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ) {

            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.CALL_PHONE},
                    1);
        }
        else
        {
            String uri = "tel:" + telp.trim() ;
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1 : {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    call();

                } else {

                }
                return;
            }

        }
    }

    public void chatMethod()
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Pilih metode : ");

        builder.setPositiveButton("SMS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address", telp);
                startActivity(smsIntent);
            }
        });

        builder.setNegativeButton("Email", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"+email));
                startActivity(Intent.createChooser(intent, "Pilih email anda"));
            }
        });

        dialog = builder.create();

        dialog.show();

    }

    public void setSlider(ArrayList<Post> listNews){

        for(Post newsdiskon : listNews){
            TextSliderView textSliderView = new TextSliderView(this);
            if(newsdiskon.getType().equalsIgnoreCase("promo"))
            {
                textSliderView
                        .description(newsdiskon.getContent())
                        .title(newsdiskon.getTitle())
                        .image("http://kasir.yamiepanda.com/"+newsdiskon.getPhoto().replace("\\", "/"))
                        .setScaleType(BaseSliderView.ScaleType.Fit)
                        .setOnSliderClickListener(this);
            }
            else if(newsdiskon.getType().equalsIgnoreCase("news"))
            {
                textSliderView
                        .description(newsdiskon.getContent())
                        .title(newsdiskon.getTitle())
                        .image("http://kasir.yamiepanda.com/"+newsdiskon.getPhoto().replace("\\", "/"))
                        .setScaleType(BaseSliderView.ScaleType.Fit)
                        .setOnSliderClickListener(this);
            }

            Log.d("photo",  "http://kasir.yamiepanda.com/"+newsdiskon.getPhoto());
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("idnews",String.valueOf(newsdiskon.getId()));
            textSliderView.getBundle().putString("judul",newsdiskon.getTitle());
            textSliderView.getBundle().putString("isi",newsdiskon.getContent());
            textSliderView.getBundle().putString("photo","http://kasir.yamiepanda.com/"+newsdiskon.getPhoto());
            textSliderView.getBundle().putString("merchant",String.valueOf(newsdiskon.getBranch_id()));
            textSliderView.getBundle().putString("tgl_lengkap",newsdiskon.getCreated_at());
            textSliderView.getBundle().putString("kategori",newsdiskon.getType());
            textSliderView.getBundle().putString("diskon",newsdiskon.getDiscount());
            textSliderView.getBundle().putString("harga",newsdiskon.getPrice());

            mDemoSlider.addSlider(textSliderView);
        }

        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.DepthPage);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(this);
        mDemoSlider.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));
    }

    public void loadDataFromServer()
    {

        mList.clear();
        service = RestClient.getClient(this);
        call = service.getSlider();

        call.enqueue(new Callback<Results<Post>>() {
            @Override
            public void onResponse(Call<Results<Post>> call,  Response<Results<Post>> response) {

                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Log.d("Load Diskon Pertama", "Status Code = " + response.code());
                    Results<Post> result = response.body();
                    Log.d("Load Diskon Pertama aye", "response = " + new Gson().toJson(result));
                    mList = result.getData();
                    setSlider(mList);

                } else {
                    Log.e("Login", String.valueOf(response.raw().toString()));
                    //pDialog.dismiss();
                    //Toast.makeText(getApplicationContext(),"Email atau Password anda salah", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<Results<Post>> call, Throwable t) {
                mList.clear();
                Log.d("MainActivity", "gagalall");
                Snackbar mSnackbar = Snackbar.make(mDemoSlider, R.string.cekkoneksi, Snackbar.LENGTH_INDEFINITE)
                        .setAction("Retry", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                loadDataFromServer();
                                getProfil();
                            }
                        })
                        .setActionTextColor(Color.WHITE);

                mSnackbar.show();
            }
        });
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

        if( slider.getBundle().get("kategori").toString().equalsIgnoreCase("news")) {
            Intent i = new Intent(getApplicationContext(), DetilHotNewsActivity.class);

            Bundle b = new Bundle();
            b.putString("idnews", slider.getBundle().get("idnews").toString());
            b.putString("judul", slider.getBundle().get("judul").toString());
            b.putString("isi", slider.getBundle().get("isi").toString());
            b.putString("photo", slider.getBundle().get("photo").toString());
            b.putString("merchant", slider.getBundle().get("merchant").toString());
            b.putString("tgl_lengkap", slider.getBundle().get("tgl_lengkap").toString());

            i.putExtras(b);
            startActivity(i);
        }
        else{
            Intent i = new Intent(getApplicationContext(), DetilPromoActivity.class);

            Bundle b = new Bundle();
            b.putString("idnews", slider.getBundle().get("idnews").toString());
            b.putString("judul", slider.getBundle().get("judul").toString());
            b.putString("isi", slider.getBundle().get("isi").toString());
            b.putString("photo", slider.getBundle().get("photo").toString());
            b.putString("merchant", slider.getBundle().get("merchant").toString());
            b.putString("tgl_lengkap", slider.getBundle().get("tgl_lengkap").toString());
            b.putString("diskon", slider.getBundle().get("diskon").toString());
            b.putString("harga", slider.getBundle().get("harga").toString());

            i.putExtras(b);
            startActivity(i);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onPause() {
        //Saat onPause, BroadcastRecievernya akan di unregister.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("TAG", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    public void checkDevice(String token)
    {
        RestClient.GitApiInterface service = RestClient.getClient(this);
        TelephonyManager tManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String uuid = "aa";//tManager.getDeviceId();
        Call<Result<Update>> call = service.checkDevice(uuid,"2.0", Build.MANUFACTURER,android.os.Build.MODEL,"Android ",android.os.Build.VERSION.RELEASE,token);

        call.enqueue(new Callback<Result<Update>>() {
            @Override
            public void onResponse(Call<Result<Update>> call,Response<Result<Update>> response) {
                Log.d("Update Device", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    final Result<Update> result = response.body();
                    Log.d("Update Device", "response = " + new Gson().toJson(result));

                    if (result.getSuccess()) {

                        if(!result.getData().getVersion_code().equalsIgnoreCase("2.0") && !result.getData().getVersion_code().equalsIgnoreCase(""))
                        {
                            if(result.getData().getRequired())
                            {
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setMessage(result.getData().getDescription());

                                builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//                                        try {
//                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.lanayacomputindo.vcm.vcm_apps_layanacomputindo")));
//                                        } catch (android.content.ActivityNotFoundException anfe) {
//                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + "com.lanayacomputindo.vcm.vcm_apps_layanacomputindo")));
//                                        }
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(result.getData().getUrl())));
                                    }
                                });

                                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });

                                dialog = builder.create();

                                dialog.show();

                                Button nbutton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                                nbutton.setTextColor(Color.RED);
                                Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                                pbutton.setTextColor(getResources().getColor(R.color.colorPrimary));
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(), response.body().getData().getDescription(), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                    else {
                        checkDevice("");
                        Intent i = new Intent(getApplicationContext(), SplashActivity.class);
                        startActivity(i);
                        finish();
                        Log.e("Update Device", response.body().getMessage());
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Log.e("Update Device", String.valueOf(response.raw().toString()));
                    //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Result<Update>> call,Throwable t) {
                Log.e("on Failure", t.toString());
                Log.d("update gcm gagal", "dicoba cek cek cek");
            }
        });
    }


    public void getProfil()
    {
        RestClient.GitApiInterface service = RestClient.getClient(this);
        Call<Result<User>> call = service.getProfil();

        call.enqueue(new Callback<Result<User>>() {
            @Override
            public void onResponse(Call<Result<User>> call,Response<Result<User>> response) {
                Log.d("Get Profil", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    Result<User> result = response.body();
                    Log.d("Get Profil", "response = " + new Gson().toJson(result));
                    if(result.getSuccess())
                    {
                        savePreferences(true,
                                result.getData().getId(),
                                result.getData().getClient_id(),
                                result.getData().getType(),
                                result.getData().getName(),
                                result.getData().getEmail(),
                                result.getData().getPoint(),
                                result.getData().getBirthday(),
                                result.getData().getReligion(),
                                result.getData().getPhone(),
                                result.getData().getAddress(),
                                result.getData().getPhoto(),
                                result.getData().getStatus(),
                                result.getData().getAccess(),
                                result.getData().getCreated_at(),
                                result.getData().getUpdated_at());

                    }
                    else {
                        Log.e("Get Profil", response.body().getMessage());
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Log.e("Get Profil", String.valueOf(response.raw().toString()));
                    Toast.makeText(getApplicationContext(),"Tidak Dapat", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Result<User>> call,Throwable t) {
                Log.e("on Failure", t.toString());
                Toast.makeText(getApplicationContext(),R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }

}

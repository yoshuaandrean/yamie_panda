package com.vcm_member20.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;
import com.vcm_member20.R;
import com.vcm_member20.rest.RestClient;

import de.hdodenhof.circleimageview.CircleImageView;

public class PengaturanActivity extends AppCompatActivity{

    private LinearLayout after;
    private TextView keluar, nama, email, poin;
    private CircleImageView image;
    private ImageView background;

    private Switch berita, produk;

    private SharedPreferences sharedPreferences;

    private void savePreferences(boolean islogin) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("islogin", islogin);
        editor.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("");

        berita = (Switch) findViewById(R.id.swberita);
        produk = (Switch) findViewById(R.id.swproduk);

        image = (CircleImageView) findViewById(R.id.image);
        background = (ImageView) findViewById(R.id.background);
        nama = (TextView) findViewById(R.id.nama);
        email = (TextView) findViewById(R.id.email);
        poin = (TextView) findViewById(R.id.poin);

        after = (LinearLayout) findViewById(R.id.after);
        keluar = (TextView) findViewById(R.id.keluar);
        keluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //checkDevice("");
                Intent i = new Intent(getApplicationContext(), SplashActivity.class);
                startActivity(i);
                MainActivity.act.finish();
                finish();
                savePreferences(false);
            }
        });
        keluar.setVisibility(View.GONE);

        sharedPreferences = getSharedPreferences("shared", Activity.MODE_PRIVATE);
        background.setBackgroundResource(R.drawable.header);

        after.setVisibility(View.VISIBLE);
        keluar.setVisibility(View.VISIBLE);
        berita.setEnabled(true);
        produk.setEnabled(true);
        Typeface bold = Typeface.createFromAsset(getApplicationContext().getAssets(),  "Quicksand-Bold.otf");
        nama.setTypeface(bold);
        email.setTypeface(bold);
        poin.setTypeface(bold);
        if(sharedPreferences.getString("status", "").equalsIgnoreCase("1")){
            nama.setText(sharedPreferences.getString("name", ""));
        }
        else{
            nama.setText(sharedPreferences.getString("name", "") + " " + "(Premium)");
        }

        email.setText(sharedPreferences.getString("email", ""));
        poin.setText("Poin : "+sharedPreferences.getString("poin", ""));
        Picasso.with(this).load("http://kasir.yamiepanda.com/" + sharedPreferences.getString("photo", "").replace("\\", "/"))
                .error(R.drawable.loadimage)
                .placeholder(R.drawable.loadimage)
                .into(image);

    }

//    public void checkDevice(String token)
//    {
//        RestClient.GitApiInterface service = RestClient.getClient(this);
//        TelephonyManager tManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
//        String uuid = "aa";//tManager.getDeviceId();
//        Call<Result<Update>> call = service.checkDevice(uuid,String.valueOf(BuildConfig.VERSION_NAME),Build.MANUFACTURER,android.os.Build.MODEL,"Android ",android.os.Build.VERSION.RELEASE,token);
//
//        call.enqueue(new Callback<Result<Update>>() {
//            @Override
//            public void onResponse(Call<Result<Update>> call,Response<Result<Update>> response) {
//                Log.d("Update Device", "Status Code = " + response.code());
//                if (response.isSuccessful()) {
//                    // request successful (status code 200, 201)
//                    final Result<Update> result = response.body();
//                    Log.d("Update Device", "response = " + new Gson().toJson(result));
//
//                    if (result.getSuccess()) {
//                        saveServerKey(result.getData().getGcm_server());
//                        if(!result.getData().getVersion_code().equalsIgnoreCase(BuildConfig.VERSION_NAME) && !result.getData().getVersion_code().equalsIgnoreCase(""))
//                        {
//                            if(result.getData().getRequired())
//                            {
//                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//                                builder.setMessage(result.getData().getDescription());
//
//                                builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
////                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
////                                        try {
////                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.lanayacomputindo.vcm.vcm_apps_layanacomputindo")));
////                                        } catch (android.content.ActivityNotFoundException anfe) {
////                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + "com.lanayacomputindo.vcm.vcm_apps_layanacomputindo")));
////                                        }
//                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(result.getData().getUrl())));
//                                    }
//                                });
//
//                                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        finish();
//                                    }
//                                });
//
//                                dialog = builder.create();
//
//                                dialog.show();
//
//                                Button nbutton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
//                                nbutton.setTextColor(Color.RED);
//                                Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
//                                pbutton.setTextColor(getResources().getColor(R.color.warna_utama));
//                            }
//                            else
//                            {
//                                Toast.makeText(getApplicationContext(), response.body().getData().getDescription(), Toast.LENGTH_LONG).show();
//                            }
//                        }
//                    }
//                    else {
//                        checkDevice("");
//                        Intent i = new Intent(getApplicationContext(), SplashActivity.class);
//                        startActivity(i);
//                        finish();
//                        savePreferences(false, false);
//                        Log.e("Update Device", response.body().getMessage());
//                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
//                    }
//
//                } else {
//                    Log.e("Update Device", String.valueOf(response.raw().toString()));
//                    //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Result<Update>> call,Throwable t) {
//                Log.e("on Failure", t.toString());
//                Log.d("update gcm gagal", "dicoba cek cek cek");
//            }
//        });
//    }

    @Override
    protected void onResume() {
        super.onResume();
        nama.setText(sharedPreferences.getString("name", ""));
        email.setText(sharedPreferences.getString("email", ""));
        Picasso.with(this).load("http://kasir.yamiepanda.com/" + sharedPreferences.getString("photo", "").replace("\\", "/"))
                .error(R.drawable.loadimage)
                .placeholder(R.drawable.loadimage)
                .into(image);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_profil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case R.id.menu_setting:

                Intent i = new Intent(getApplicationContext(), EditProfileActivity.class);
                startActivity(i);

                return true;

            case android.R.id.home:

                finish();

                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}

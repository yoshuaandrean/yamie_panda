package com.vcm_member20.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import com.vcm_member20.R;

public class SplashActivity extends AppCompatActivity {
    private boolean islogin;

    private SharedPreferences sharedPreferences;

    public void loadPreferences() {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            islogin = sharedPreferences.getBoolean("islogin", false);
        } else {
            islogin = false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadPreferences();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Log.d("statuslogin",String.valueOf(islogin));


                if(islogin)
                {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);

                    finish();
                }
                else {
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(i);

                    finish();
                }

            }
        }, 2000);
    }
}

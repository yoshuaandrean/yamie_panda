package com.vcm_member20.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.google.gson.Gson;
import com.vcm_member20.R;
import com.vcm_member20.model.Result;
import com.vcm_member20.model.User;
import com.vcm_member20.rest.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ScanQrCodeActivity extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener {

    private QRCodeReaderView mydecoderview;

    public int flag=0;

    private ProgressDialog pDialog;

    private String id_promo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qr_code);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        mydecoderview = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
        mydecoderview.setOnQRCodeReadListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        if(flag==0) {
            id_promo = text;
            scanProfile(id_promo);
            flag=1;
        }
    }


    @Override
    public void cameraNotFound() {
        Toast.makeText(this, "Device anda tidak mempunyai kamera", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void QRCodeNotFoundOnCamImage() {
        //Toast.makeText(this, "Qr Code tidak ditemukan", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mydecoderview.getCameraManager().startPreview();

        flag = 0;
    }

    public void scanProfile(String id_promo)
    {
        pDialog = ProgressDialog.show(this, "Proses Scan", "Tunggu Sebentar!");
        RestClient.GitApiInterface service = RestClient.getClient(this);
        Call<Result<User>> call = service.insertVoucher(id_promo);
        call.enqueue(new Callback<Result<User>>() {
            @Override
            public void onResponse(Call<Result<User>> call, Response<Result<User>> response) {
                Log.d("Login", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Result<User> result = response.body();
                    Log.d("Login", "response = " + new Gson().toJson(result));
                    if(result.getSuccess())
                    {
                        finish();
                        pDialog.dismiss();
                        Toast.makeText(ScanQrCodeActivity.this, "Promo sudah berhasil discan", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Log.e("Login", response.body().getMessage());
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Log.e("Login", String.valueOf(response.raw().toString()));
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Anda sudah pernah mendapatkan Promo ini", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Result<User>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Toast.makeText(getApplicationContext(),R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mydecoderview.getCameraManager().stopPreview();
    }
}

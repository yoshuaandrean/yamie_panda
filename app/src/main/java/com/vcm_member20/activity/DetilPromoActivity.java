package com.vcm_member20.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderViewKenBurn;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.example.captain_miao.grantap.CheckPermission;
import com.example.captain_miao.grantap.listeners.PermissionListener;
import com.google.gson.Gson;
import com.vcm_member20.R;
import com.vcm_member20.model.Results;
import com.vcm_member20.model.Share;
import com.vcm_member20.rest.RestClient;
import com.vcm_member20.util.TimeFormater;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetilPromoActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{

    private TextView txtdeskripsi, txtnama, txttanggal;

    private String bundleid, bundlejudul, bundleisi, bundlelengkap, bundlephoto, share = "";

    private Button btnAmbilPromo;

    private SliderLayout mDemoSlider;

    private CollapsingToolbarLayout collapsingToolbarLayout;

    private SharedPreferences sharedPreferences;

    private String[] permissioncamera = new String[]{Manifest.permission.CAMERA};

    private int flagpromo = 0;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_diskon);

        mDemoSlider = (SliderLayout)findViewById(R.id.slider);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        sharedPreferences = getSharedPreferences("shared", Activity.MODE_PRIVATE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        Bundle b = getIntent().getExtras();
        bundleid = b.getString("idnews");
        bundlejudul = b.getString("judul");
        bundleisi = b.getString("isi");
        bundlephoto = b.getString("photo").replace("\\", "/");
        bundlelengkap = b.getString("tgl_lengkap");

        getSupportActionBar().setTitle(bundlejudul);

        Typeface bold = Typeface.createFromAsset(getApplicationContext().getAssets(),  "Quicksand-Bold.otf");

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(bundlejudul);
        collapsingToolbarLayout.setCollapsedTitleTypeface(bold);
        collapsingToolbarLayout.setExpandedTitleTypeface(bold);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        txtdeskripsi = (TextView) findViewById(R.id.deskripsi);
        txtnama = (TextView) findViewById(R.id.nama);
        txttanggal = (TextView) findViewById(R.id.tanggal);
        txtnama.setText(bundlejudul);
        txtnama.setTypeface(bold);
        txtdeskripsi.setText(bundleisi);
        txtdeskripsi.setTypeface(bold);
        txttanggal.setText(new TimeFormater().formattedDateFromString("yyyy-MM-dd", "dd MMMM yyyy", bundlelengkap));
        txttanggal.setTypeface(bold);

        btnAmbilPromo = (Button) findViewById(R.id.btnAmbilPromo);
        btnAmbilPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(flagpromo == 0){
                    setDialogShare();
                }
                else{
                    Intent i = new Intent(getApplicationContext(), ScanQrCodeActivity.class);
                    startActivity(i);
                }
            }
        });

        TextSliderViewKenBurn textSliderView = new TextSliderViewKenBurn(this);
        textSliderView
                .image(bundlephoto)
                .setScaleType(BaseSliderView.ScaleType.Fit)
                .setOnSliderClickListener(this);

        Log.d("photo", bundlephoto);
        textSliderView.bundle(new Bundle());
        textSliderView.getBundle()
                .putString("photo",bundlephoto);

        mDemoSlider.addSlider(textSliderView);
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setDuration(60000);
        mDemoSlider.addOnPageChangeListener(this);
        mDemoSlider.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));


        getShare();
    }

    public void getShare()
    {
        pDialog = ProgressDialog.show(this,
                "",
                "Tunggu Sebentar!");
        RestClient.GitApiInterface service = RestClient.getClient(this);
        Call<Results<Share>> call = service.getShare();

        call.enqueue(new Callback<Results<Share>>() {
            @Override
            public void onResponse(Call<Results<Share>> call, Response<Results<Share>> response) {
                pDialog.dismiss();
                if (response.isSuccessful()) {
                    Results<Share> result = response.body();
                    Log.d("Get Share", "response = " + new Gson().toJson(result));
                    if(result.getData().size()>0){
                        share = result.getData().get(0).getName();
                    }
                } else {
                    Log.e("Get Share", String.valueOf(response.raw().toString()));
                    Toast.makeText(getApplicationContext(),"Tidak Dapat", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Results<Share>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Toast.makeText(getApplicationContext(),R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void setupShareIntent() {

        Uri bmpUri = getLocalBitmapUri(TextSliderViewKenBurn.target);

        Intent emailIntent = new Intent();
        emailIntent.setAction(Intent.ACTION_SEND);
        emailIntent.setType("");

        PackageManager pm = getPackageManager();
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("image/*");


        Intent openInChooser = Intent.createChooser(emailIntent, "Silahkan Pilih Metode Share");

        List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
        List<LabeledIntent> intentList = new ArrayList<>();
        for (int i = 0; i < resInfo.size(); i++) {
            // Extract the label, append it, and repackage it in a LabeledIntent
            ResolveInfo ri = resInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            if(packageName.contains("android.email")) {
                emailIntent.setPackage(packageName);
            } else if(packageName.contains("twitter") || packageName.contains("facebook") || packageName.contains("line") || packageName.contains("instagram")) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                if(packageName.contains("twitter")) {
                    intent.putExtra(Intent.EXTRA_TEXT, "twiter");
                } else if(packageName.contains("facebook")) {
                    // Warning: Facebook IGNORES our text. They say "These fields are intended for users to express themselves. Pre-filling these fields erodes the authenticity of the user voice."
                    // One workaround is to use the Facebook SDK to post, but that doesn't allow the user to choose how they want to share. We can also make a custom landing page, and the link
                    // will show the <meta content ="..."> text from that page with our link in Facebook.
                    intent.putExtra(Intent.EXTRA_TEXT, "facebook");
                } else if(packageName.contains("line")) {
                    intent.putExtra(Intent.EXTRA_TEXT, "line");
                } else if(packageName.contains("instagram")) { // If Gmail shows up twice, try removing this else-if clause and the reference to "android.gm" above
                    intent.putExtra(Intent.EXTRA_TEXT, "instagram");
                    intent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                    intent.setType("image/*");
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }

                intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
            }
        }

        // convert intentList to array
        LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);

        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
        startActivity(openInChooser);

//        shareIntent = new Intent();
//        shareIntent.setAction(Intent.ACTION_SEND);
//        shareIntent.putExtra(Intent.EXTRA_TEXT, bundlejudul);
//        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
//        shareIntent.setType("image/*");
//        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        startActivity(Intent.createChooser(shareIntent, "Share ke media sosial . ."));
        flagpromo = 1;
    }

    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }

        Uri bmpUri = null;
        try {
            File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public void setDialogShare(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inf = LayoutInflater.from(this);
        View subView = inf.inflate(R.layout.dialog_share, null);
        builder.setView(subView);

        AlertDialog mDialog = builder.create();
        //mDialog.setCancelable(false);

        LinearLayout llfacebook = (LinearLayout) subView.findViewById(R.id.llFacebook);
        LinearLayout llLine = (LinearLayout) subView.findViewById(R.id.llLine);
        LinearLayout llInstagram = (LinearLayout) subView.findViewById(R.id.llInstagram);

        llfacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri bmpUri = getLocalBitmapUri(TextSliderViewKenBurn.target);

                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("image/*");
                shareIntent.putExtra(Intent.EXTRA_STREAM,bmpUri);
                shareIntent.putExtra(Intent.EXTRA_TEXT,bundlejudul);
                shareIntent.setPackage("com.facebook.katana");
                startActivity(Intent.createChooser(shareIntent, "Share ke Facebook"));

                flagpromo = 1;
            }
        });

        llLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri bmpUri = getLocalBitmapUri(TextSliderViewKenBurn.target);

                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("image/*");
                shareIntent.putExtra(Intent.EXTRA_STREAM,bmpUri);
                shareIntent.putExtra(Intent.EXTRA_TEXT,bundlejudul);
                shareIntent.setPackage("jp.naver.line.android");
                startActivity(Intent.createChooser(shareIntent, "Share ke Line"));

                flagpromo = 1;
            }
        });

        llInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri bmpUri = getLocalBitmapUri(TextSliderViewKenBurn.target);

                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("image/*");
                shareIntent.putExtra(Intent.EXTRA_STREAM,bmpUri);
                shareIntent.putExtra(Intent.EXTRA_TEXT,bundlejudul);
                shareIntent.setPackage("com.instagram.android");
                startActivity(Intent.createChooser(shareIntent, "Share ke Instagram"));

                flagpromo = 1;
            }
        });

        llfacebook.setVisibility(View.GONE);
        llLine.setVisibility(View.GONE);
        llInstagram.setVisibility(View.GONE);

        if(share.equalsIgnoreCase("Facebook")){
            llfacebook.setVisibility(View.VISIBLE);
        }
        if(share.equalsIgnoreCase("Line")){
            llLine.setVisibility(View.VISIBLE);
        }
        if(share.equalsIgnoreCase("Instagram")){
            llInstagram.setVisibility(View.VISIBLE);
        }

        mDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                finish();

                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}

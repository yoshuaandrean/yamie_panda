package com.vcm_member20.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vcm_member20.R;
import com.vcm_member20.model.Result;
import com.vcm_member20.model.Token;
import com.vcm_member20.model.User;
import com.vcm_member20.rest.RestClient;
import com.vcm_member20.util.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Button btnmasuk;
    private TextView daftar, lupa;

    private EditText edemail, edpassword;

    public static Activity ActLogin;
    private ProgressDialog pDialog;

    private SharedPreferences sharedPreferences;

    private AlertDialog dialog;

    private void savePreferences(boolean islogin,
                                 int id,
                                 int client_id,
                                 String type,
                                 String name,
                                 String email,
                                 String poin,
                                 String birthday,
                                 String religion,
                                 String phone,
                                 String address,
                                 String photo,
                                 String status,
                                 String access,
                                 String created_at,
                                 String updated_at) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("islogin", islogin);
        editor.putInt("id", id);
        editor.putInt("client_id", client_id);
        editor.putString("type", type);
        editor.putString("name", name);
        editor.putString("email", email);
        editor.putString("poin", poin);
        editor.putString("birthday", birthday);
        editor.putString("religion", religion);
        editor.putString("phone", phone);
        editor.putString("address", address);
        editor.putString("photo", photo);
        editor.putString("status", status);
        editor.putString("access", access);
        editor.putString("created_at", created_at);
        editor.putString("updated_at", updated_at);
        editor.commit();
    }

    private void saveToken(String user_token) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user_token", user_token);
        editor.commit();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ActLogin = this;

        edemail = (EditText) findViewById(R.id.edemail);
        edpassword = (EditText) findViewById(R.id.edpassword);

        btnmasuk = (Button) findViewById(R.id.btnmasuk);

        btnmasuk.getBackground().setAlpha(150);

        btnmasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ceklogin();
            }
        });

        daftar = (TextView) findViewById(R.id.daftar);
        SpannableString content = new SpannableString("Daftar Akun");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        daftar.setText(content);
        lupa = (TextView) findViewById(R.id.lupa);
        SpannableString content1 = new SpannableString("Lupa Password");
        content1.setSpan(new UnderlineSpan(), 0, content1.length(), 0);
        lupa.setText(content1);

        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
            }
        });

        lupa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }
    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private void ceklogin()
    {
        boolean cancel = true;

        String email = edemail.getText().toString();
        String password = edpassword.getText().toString();

        if (email.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(),R.string.emptyemail, Toast.LENGTH_LONG).show();
            cancel = false;
        } else if (!isEmailValid(email)) {
            Toast.makeText(getApplicationContext(),R.string.invalidemail, Toast.LENGTH_LONG).show();
            cancel = false;
        } else  if (password.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(),R.string.emptypassword, Toast.LENGTH_LONG).show();
            cancel = false;
        }

        if (cancel) {
            pDialog = ProgressDialog.show(LoginActivity.this,
                    "Login",
                    "Tunggu Sebentar!");


            getToken(email,password);
            //getTokenDana(email,password);

        }

    }

    public void getToken(String email, String password)
    {

        RestClient.GitApiInterface service = RestClient.getClient(this);
        Call<Result<Token>> call = service.getToken(Constants.client_id, email,password);
        call.enqueue(new Callback<Result<Token>>() {
            @Override
            public void onResponse(Call<Result<Token>> call, Response<Result<Token>> response) {
                Log.d("Login", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Result<Token> result = response.body();
                    Log.d("Login", "response = " + new Gson().toJson(result));
                    if(result.getSuccess())
                    {
                        saveToken(result.getData().getToken());
                        Log.d("Login", "Token: " + response.body().getData().getToken());
                        getProfil();
                    }
                    else {
                        Log.e("Login", response.body().getMessage());
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Log.e("Login", String.valueOf(response.raw().toString()));
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Email atau Password anda salah", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Result<Token>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Toast.makeText(getApplicationContext(),R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getProfil()
    {
        RestClient.GitApiInterface service = RestClient.getClient(this);
        Call<Result<User>> call = service.getProfil();

        call.enqueue(new Callback<Result<User>>() {
            @Override
            public void onResponse(Call<Result<User>> call,Response<Result<User>> response) {
                Log.d("Get Profil", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    Result<User> result = response.body();
                    Log.d("Get Profil", "response = " + new Gson().toJson(result));
                    if(result.getSuccess())
                    {
                        savePreferences(true,
                                result.getData().getId(),
                                result.getData().getClient_id(),
                                result.getData().getType(),
                                result.getData().getName(),
                                result.getData().getEmail(),
                                result.getData().getPoint(),
                                result.getData().getBirthday(),
                                result.getData().getReligion(),
                                result.getData().getPhone(),
                                result.getData().getAddress(),
                                result.getData().getPhoto(),
                                result.getData().getStatus(),
                                result.getData().getAccess(),
                                result.getData().getCreated_at(),
                                result.getData().getUpdated_at());

                        pDialog.dismiss();
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();

                    }
                    else {
                        Log.e("Get Profil", response.body().getMessage());
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Log.e("Get Profil", String.valueOf(response.raw().toString()));
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Tidak Dapat", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Result<User>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Toast.makeText(getApplicationContext(),R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void showDialog()
    {
        LayoutInflater inflater = LayoutInflater.from(LoginActivity.this);
        View subView = inflater.inflate(R.layout.dialoglupapassword, null);
        final EditText edemail = (EditText)subView.findViewById(R.id.email);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Masukkan email anda : ");
        builder.setView(subView);

        builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                lupapassword(edemail.getText().toString());

            }
        });

        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        dialog = builder.create();

        dialog.show();

        Button nbutton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(Color.RED);
        Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    public void lupapassword(String email)
    {
        pDialog = ProgressDialog.show(LoginActivity.this,
                "Mengirim Perubahan Password Baru Anda",
                "Tunggu Sebentar!");

        RestClient.GitApiInterface service = RestClient.getClient(this);
        Call<Result<User>> call = service.forgotPassword(email);

        call.enqueue(new Callback<Result<User>>() {
            @Override
            public void onResponse(Call<Result<User>> call,Response<Result<User>> response) {
                Log.d("MainActivity", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Silahkan cek email anda", Toast.LENGTH_LONG).show();

                } else {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Email anda tidak terdaftar", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<Result<User>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Toast.makeText(getApplicationContext(),R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }

}

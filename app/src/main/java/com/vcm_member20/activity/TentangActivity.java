package com.vcm_member20.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderViewKenBurn;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.example.captain_miao.grantap.CheckPermission;
import com.example.captain_miao.grantap.listeners.PermissionListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.vcm_member20.R;
import com.vcm_member20.model.Branch;
import com.vcm_member20.model.Client;
import com.vcm_member20.model.Result;
import com.vcm_member20.rest.RestClient;
import com.vcm_member20.util.FlatingActionImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TentangActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener  {
    private Intent shareIntent;

    private TextView txtisi, txtabout, txtalamat, txtnama;

    private FlatingActionImageView img;

    private SliderLayout mDemoSlider;

    private CollapsingToolbarLayout collapsingToolbarLayout;

    private RelativeLayout rlcall;

    private String telp;


    private static GoogleMap mMap;

    private final int[] MAP_TYPES = { GoogleMap.MAP_TYPE_SATELLITE,
            GoogleMap.MAP_TYPE_NORMAL,
            GoogleMap.MAP_TYPE_HYBRID,
            GoogleMap.MAP_TYPE_TERRAIN,
            GoogleMap.MAP_TYPE_NONE };

    private GoogleApiClient googleApiClient;
    private LocationManager locManager;
    private Location currentLoc;

    private Call<Result<Client>> callmap;
    private RestClient.GitApiInterface servicemap;
    private static ArrayList<Branch> listBranch = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        mDemoSlider = (SliderLayout)findViewById(R.id.slider);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        rlcall = (RelativeLayout) findViewById(R.id.rlcall);
        rlcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                call();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        getSupportActionBar().setTitle("");

        Typeface bold = Typeface.createFromAsset(getApplicationContext().getAssets(),  "Quicksand-Bold.otf");

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle("");
        collapsingToolbarLayout.setCollapsedTitleTypeface(bold);
        collapsingToolbarLayout.setExpandedTitleTypeface(bold);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        img = (FlatingActionImageView) findViewById(R.id.img);

        txtisi = (TextView) findViewById(R.id.isi);
        txtnama = (TextView) findViewById(R.id.nama);
        txtalamat = (TextView) findViewById(R.id.alamat);
        txtabout = (TextView) findViewById(R.id.about);
        txtisi.setTypeface(bold);
        txtnama.setTypeface(bold);
        txtalamat.setTypeface(bold);
        txtabout.setTypeface(bold);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        googleApiClient = new GoogleApiClient.Builder(TentangActivity.this)
                .addConnectionCallbacks(TentangActivity.this)
                .addOnConnectionFailedListener(TentangActivity.this)
                .addApi(LocationServices.API)
                .build();

    }

    public void setupShareIntent() {
        Uri bmpUri = getLocalBitmapUri(TextSliderViewKenBurn.target);
        shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, "");
        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, "Share ke media sosial . ."));
    }

    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }

        Uri bmpUri = null;
        try {
            File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                finish();

                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                return true;

            case R.id.menu_item_share:

                setupShareIntent();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        setTokoInMap();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        // Add a marker in Sydney and move the camera
        CheckPermission
                .from(TentangActivity.this)
                .setPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION})
                .setRationaleConfirmText("Meminta ijin deteksi likasi anda")
                .setDeniedMsg("The Location Denied")
                .setPermissionListener(new PermissionListener() {

                    @Override
                    public void permissionGranted() {
                        googleApiClient.connect();
                    }

                    @Override
                    public void permissionDenied() {
                        Toast.makeText(getApplicationContext(), "Lokasi tidak diijinkan", Toast.LENGTH_SHORT).show();
                    }
                })
                .check();
    }

    public void setTokoInMap()
    {
        try {

            if(netCheckin()) {

                locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

                if ( !locManager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                    buildAlertMessageNoGps();
                }
                else {

                    mMap.clear();
                    mMap.setMapType(MAP_TYPES[1]);
                    mMap.setTrafficEnabled(true);
                    mMap.getUiSettings().setZoomControlsEnabled(true);

                    servicemap = RestClient.getClient(this);
                    callmap = servicemap.getProfilMerchant("branch");

                    callmap.enqueue(new Callback<Result<Client>>() {
                        @Override
                        public void onResponse(Call<Result<Client>> call, Response<Result<Client>> response) {
                            Log.d("MainActivity", "Status Code = " + response.code());
                            if (response.isSuccessful()) {
                                // request successful (status code 200, 201)
                                Result<Client> result = response.body();
                                Log.d("MainActivity", "response = " + new Gson().toJson(result));
                                listBranch = result.getData().getBranch();
                                for (int i = 0; i < listBranch.size(); i++) {
                                    if(!listBranch.get(i).getLatitude().equalsIgnoreCase("")&&!listBranch.get(i).getLongitude().equalsIgnoreCase("")){
                                        LatLng posisitoko = new LatLng(Double.parseDouble(listBranch.get(i).getLatitude()), Double.parseDouble(listBranch.get(i).getLongitude()));
                                        mMap.addMarker(new MarkerOptions().position(posisitoko).title(listBranch.get(i).getName()).snippet(listBranch.get(i).getAddress()).icon(BitmapDescriptorFactory.fromResource(R.drawable.map)));
                                        Log.d("TextLokasi", listBranch.get(i).getName());
                                        if(i==0)
                                        {
                                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(posisitoko, 15));
                                            mMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
                                        }
                                    }
                                }

                                txtnama.setText(result.getData().getName());
                                txtalamat.setText(Html.fromHtml(result.getData().getAddress()));
                                txtisi.setText(Html.fromHtml(result.getData().getDescription()));
                                txtabout.setText("About "+result.getData().getName());
                                telp = result.getData().getPhone();

                                TextSliderViewKenBurn textSliderView = new TextSliderViewKenBurn(TentangActivity.this);
                                textSliderView
                                        .image("http://kasir.yamiepanda.com/"+result.getData().getBackground().replace("\\", "/"))
                                        .setScaleType(BaseSliderView.ScaleType.Fit)
                                        .setOnSliderClickListener(TentangActivity.this);

                                textSliderView.bundle(new Bundle());
                                textSliderView.getBundle()
                                        .putString("photo","http://kasir.yamiepanda.com/"+result.getData().getBackground().replace("\\", "/"));

                                mDemoSlider.addSlider(textSliderView);
                                mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
                                mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                                mDemoSlider.setDuration(60000);
                                mDemoSlider.addOnPageChangeListener(TentangActivity.this);
                                mDemoSlider.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));


                            } else {
                                // response received but request not successful (like 400,401,403 etc)
                                //Handle errors
                                Log.e("on Failure", "asas");

                            }
                        }

                        @Override
                        public void onFailure(Call<Result<Client>> call, Throwable t) {
                            Log.d("MainActivity", "gagalall");
                            Log.e("on Failure", t.toString());
                            Toast.makeText(getApplicationContext(),R.string.cekkoneksi,Toast.LENGTH_LONG).show();
                        }
                    });


                }
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Cek kembali koneksi anda", Toast.LENGTH_LONG).show();
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Toast.makeText(getApplicationContext(),"Tunggu beberapa saat lagi",Toast.LENGTH_LONG).show();
        }
    }

    private boolean netCheckin() {
        try {
            ConnectivityManager nInfo = (ConnectivityManager) getSystemService(
                    Context.CONNECTIVITY_SERVICE);
            nInfo.getActiveNetworkInfo().isConnectedOrConnecting();
            Log.d("", "Net avail:"
                    + nInfo.getActiveNetworkInfo().isConnectedOrConnecting());
            ConnectivityManager cm = (ConnectivityManager) getSystemService(
                    Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                Log.d("", "Network available:true");
                return true;
            } else {
                Log.d("", "Network available:false");
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("GPS anda tidak aktif, aktifkan sekarang?")
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void  call(){
        if ( ContextCompat.checkSelfPermission(TentangActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ) {

            ActivityCompat.requestPermissions(TentangActivity.this, new String[]{android.Manifest.permission.CALL_PHONE},
                    1);
        }
        else
        {
            String uri = "tel:" + telp.trim() ;
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1 : {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    call();

                } else {

                }
                return;
            }

        }
    }
}

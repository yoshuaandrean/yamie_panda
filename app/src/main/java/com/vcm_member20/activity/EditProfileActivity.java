package com.vcm_member20.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.captain_miao.grantap.CheckPermission;
import com.example.captain_miao.grantap.listeners.PermissionListener;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.vcm_member20.R;
import com.vcm_member20.model.Result;
import com.vcm_member20.model.User;
import com.vcm_member20.rest.RestClient;
import com.vcm_member20.util.ImageConverters;
import com.vcm_member20.util.ImagePicker;
import com.vcm_member20.util.Sources;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class EditProfileActivity extends AppCompatActivity {

    private EditText edalamat, ednama, ednotelp, edlahir;

    private Spinner spagama;

    private TextView txtemail;

    private Button btnsimpan, btnedpassword;

    private CircleImageView imgphoto;

    File fileimage = null;

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    private ProgressDialog pDialog;

    Bitmap imageupload;

    private String imageprofil, imageqrcode;
    //File sourceFile;

    protected static final int DATE_DIALOG_ID = 0;

    private Date date2 = new Date();
    private String tanggal;

    String[] permissioncamera = new String[]{Manifest.permission.CAMERA};

    int id,client_id;
    String name,email,birthday,religion,phone,address,photo,created_at,updated_at, status;

    private SharedPreferences sharedPreferences;

    public void loadPreferences() {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            id = sharedPreferences.getInt("id", 0);
            client_id = sharedPreferences.getInt("client_id", 0);
            name = sharedPreferences.getString("name", "");
            email = sharedPreferences.getString("email", "");
            birthday = sharedPreferences.getString("birthday", "");
            religion = sharedPreferences.getString("religion", "");
            phone = sharedPreferences.getString("phone", "");
            address = sharedPreferences.getString("address", "");
            photo = sharedPreferences.getString("photo", "");
            status = sharedPreferences.getString("status", "");
            created_at = sharedPreferences.getString("created_at", "");
            updated_at = sharedPreferences.getString("updated_at", "");
        } else {

        }
    }

    private void savePreferences(boolean islogin,
                                 int id,
                                 int client_id,
                                 String name,
                                 String email,
                                 String poin,
                                 String birthday,
                                 String religion,
                                 String phone,
                                 String address,
                                 String photo,
                                 String status,
                                 String access,
                                 String created_at,
                                 String updated_at) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("islogin", islogin);
        editor.putInt("id", id);
        editor.putInt("client_id", client_id);
        editor.putString("name", name);
        editor.putString("email", email);
        editor.putString("poin", poin);
        editor.putString("birthday", birthday);
        editor.putString("religion", religion);
        editor.putString("phone", phone);
        editor.putString("address", address);
        editor.putString("photo", photo);
        editor.putString("status", status);
        editor.putString("access", access);
        editor.putString("created_at", created_at);
        editor.putString("updated_at", updated_at);
        editor.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.getBackground().setAlpha(95);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        loadPreferences();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        txtemail = (TextView) findViewById(R.id.email);
        ednama = (EditText) findViewById(R.id.nama);
        edalamat = (EditText) findViewById(R.id.alamat);
        ednotelp = (EditText) findViewById(R.id.notelp);
        imgphoto = (CircleImageView) findViewById(R.id.photo);
        edlahir = (EditText) findViewById(R.id.lahir);
        spagama = (Spinner) findViewById(R.id.agama);

        edlahir.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (v == edlahir)
                    showDialog(DATE_DIALOG_ID);
                return false;
            }
        });

        btnsimpan = (Button) findViewById(R.id.btnsimpan);
        btnedpassword = (Button) findViewById(R.id.btnedpassword);

        DateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
        try {

            date2 = formatter.parse(birthday);
        }
        catch(ParseException ex){

        }

        txtemail.setText(email);
        ednama.setText(name);
        edalamat.setText(address);
        ednotelp.setText(phone);
        edlahir.setText(birthday);

        spagama.setSelection(getIndex(spagama, religion));

        String photoo = photo.replace("\\", "/");
        Picasso.with(getApplicationContext()).load("http://kasir.yamiepanda.com/" + photoo)
                .error(R.drawable.loadimage)
                .placeholder(R.drawable.loadimage)
                .into(imgphoto);

        imageupload = ((BitmapDrawable)imgphoto.getDrawable()).getBitmap();

        imgphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckPermission
                        .from(EditProfileActivity.this)
                        .setPermissions(permissioncamera)
                        .setRationaleConfirmText("Meminta ijin mengakses kamera")
                        .setDeniedMsg("The Camera Denied")
                        .setPermissionListener(new PermissionListener() {

                            @Override
                            public void permissionGranted() {
                                selectImage();
                            }

                            @Override
                            public void permissionDenied() {
                                Toast.makeText(getApplicationContext(), "Mengakses kamera tidak diijinkan", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .check();


            }
        });

        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfile();
            }
        });

        btnedpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent l = new Intent(getApplicationContext(),EditPasswordActivity.class);
                startActivity(l);
            }
        });

    }

    private int getIndex(Spinner spinner, String myString)
    {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }
        return index;
    }

    public void editProfile()
    {
        pDialog = ProgressDialog.show(EditProfileActivity.this,
                "Mengedit Profil Anda",
                "Tunggu Sebentar!");

        String image = getStringImage(imageupload);
        RestClient.GitApiInterface service = RestClient.getClient(this);

        Call<Result<User>> call = service.updateProfil(ednama.getText().toString(), ednotelp.getText().toString(), edalamat.getText().toString(), tanggal, spagama.getSelectedItem().toString(), image);

        call.enqueue(new Callback<Result<User>>() {
            @Override
            public void onResponse(Call<Result<User>> call, Response<Result<User>> response) {
                Log.d("Get Profil", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    Result<User> result = response.body();
                    Log.d("Get Profil", "response = " + new Gson().toJson(result));
                    if(result.getSuccess())
                    {
                        savePreferences(true,
                                result.getData().getId(),
                                result.getData().getClient_id(),
                                result.getData().getName(),
                                result.getData().getEmail(),
                                result.getData().getPoint(),
                                result.getData().getBirthday(),
                                result.getData().getReligion(),
                                result.getData().getPhone(),
                                result.getData().getAddress(),
                                result.getData().getPhoto(),
                                result.getData().getStatus(),
                                result.getData().getAccess(),
                                result.getData().getCreated_at(),
                                result.getData().getUpdated_at());

                        pDialog.dismiss();
                        finish();
                    }  else {
                        Log.e("Update Profil", response.body().getMessage());
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Log.e("Update Profil", String.valueOf(response.raw().toString()));
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Result<User>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Log.d("gagal", "agagagagal");
                Toast.makeText(getApplicationContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void selectImage() {
        final CharSequence[] items = { "Ambil Foto", "Pilih dari Gallery",
                "Batal" };

        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        builder.setTitle("Pilih Metode");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Ambil Foto")) {
                    pickImageFromSource(Sources.CAMERA);

                } else if (items[item].equals("Pilih dari Gallery")) {
                    pickImageFromSource(Sources.GALLERY);
                } else if (items[item].equals("Batal")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void pickImageFromSource(Sources source) {
        ImagePicker.with(this).requestImage(source)
                .flatMap(new Func1<Uri, Observable<File>>() {
                    @Override
                    public Observable<File> call(Uri uri) {
                        return ImageConverters.uriToFile(EditProfileActivity.this, uri, createTempFile());
                    }
                })
                .subscribe(new Action1<File>() {
                    @Override
                    public void call(File file) {
                        pDialog = ProgressDialog.show(EditProfileActivity.this,
                                "Mempersiapkan Foto",
                                "Tunggu Sebentar!");
                        Compressor.getDefault(EditProfileActivity.this)
                                .compressToFileAsObservable(file)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Action1<File>() {
                                    @Override
                                    public void call(File file) {
                                        pDialog.dismiss();
                                        Uri uri = Uri.fromFile(file);

                                        CropImage.activity(uri)
                                                .setGuidelines(CropImageView.Guidelines.ON)
                                                .start(EditProfileActivity.this);
                                    }
                                }, new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        pDialog.dismiss();
                                        Toast.makeText(EditProfileActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                });

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Toast.makeText(EditProfileActivity.this, String.format("Error: %s", throwable), Toast.LENGTH_LONG).show();
                    }
                });
    }

    private File createTempFile() {
        return new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                imgphoto.setImageURI(result.getUri());

                Bitmap bitmap = ((BitmapDrawable)imgphoto.getDrawable()).getBitmap();

                imageupload = bitmap;

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("error", error.getLocalizedMessage());
            }
        }
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        Calendar c = Calendar.getInstance();
        c.setTime(date2);
        int cyear = c.get(Calendar.YEAR);
        int cmonth = c.get(Calendar.MONTH);
        int cday = c.get(Calendar.DAY_OF_MONTH);

        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, mDateSetListener, cyear, cmonth, cday);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            String date_selected = String.valueOf(year)+"-"
                    + String.valueOf(monthOfYear + 1)+ "-"
                    + String.valueOf(dayOfMonth);

            String bulan = "";
            if((monthOfYear+1)==1)
            {
                bulan = "Januari";
            }
            if((monthOfYear+1)==2)
            {
                bulan = "Februari";
            }
            if((monthOfYear+1)==3)
            {
                bulan = "Maret";
            }
            if((monthOfYear+1)==4)
            {
                bulan = "April";
            }
            if((monthOfYear+1)==5)
            {
                bulan = "Mei";
            }
            if((monthOfYear+1)==6)
            {
                bulan = "Juni";
            }
            if((monthOfYear+1)==7)
            {
                bulan = "Juli";
            }
            if((monthOfYear+1)==8)
            {
                bulan = "Agustus";
            }
            if((monthOfYear+1)==9)
            {
                bulan = "September";
            }
            if((monthOfYear+1)==10)
            {
                bulan = "Oktober";
            }
            if((monthOfYear+1)==11)
            {
                bulan = "November";
            }
            if((monthOfYear+1)==12)
            {
                bulan = "Desember";
            }

            tanggal = date_selected;
            edlahir.setText(String.valueOf(dayOfMonth)+" "+bulan+" "+ String.valueOf(year));
        }
    };



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                EditProfileActivity.this.finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

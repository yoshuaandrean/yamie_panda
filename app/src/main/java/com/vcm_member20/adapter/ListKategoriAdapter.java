package com.vcm_member20.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vcm_member20.R;
import com.vcm_member20.activity.KatalogActivity;
import com.vcm_member20.fragment.FragmentKategori;
import com.vcm_member20.model.Kategori;
import com.vcm_member20.util.OnLoadMoreListener;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Edwin on 18/01/2015.
 */

public class ListKategoriAdapter{// extends RecyclerView.Adapter{
//
//    public final int VIEW_ITEM = 1;
//    public final int VIEW_PROG = 0;
//
//    public static View imgprog;
//    public static View progressprog;
//    // The minimum amount of items to have below your current scroll position
//    // before loading more.
//    public int visibleThreshold = 2;
//    public int lastVisibleItem, totalItemCount;
//    public static boolean loading;
//    public static OnLoadMoreListener onLoadMoreListener;
//
//    List<Kategori> listkategori;
//
//    private Context context;
//
//    private int lastPosition = 0;
//
//    private Kategori modelkategori;
//
//    private int screenWidth;
//
//    public static int flagprog = 0;
//
//    public static FragmentKategori fragment;
//
//    public Activity activity;
//    public ListKategoriAdapter(Context context, ArrayList<Kategori> kategori, RecyclerView recyclerView, FragmentKategori fragment, Activity activity) {
//        super();
//
//        this.fragment = fragment;
//        this.context = context;
//        this.activity = activity;
//
//        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
//        Display display = wm.getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        screenWidth = size.x;
//
//        listkategori = kategori;
//
//        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
//
//            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
//                    .getLayoutManager();
//
//            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                @Override
//                public void onScrolled(RecyclerView recyclerView,
//                                       int dx, int dy) {
//                    super.onScrolled(recyclerView, dx, dy);
//
//                    totalItemCount = linearLayoutManager.getItemCount();
//                    lastVisibleItem = linearLayoutManager
//                            .findLastVisibleItemPosition();
//
//                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
//                        // End has been reached
//                        // Do something
//                        if (onLoadMoreListener != null) {
//                            onLoadMoreListener.onLoadMore();
//                        }
//                        loading = true;
//                    }
//                }
//            });
//        }
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return listkategori.get(position) != null ? VIEW_ITEM : VIEW_PROG;
//    }
//
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        RecyclerView.ViewHolder vh;
//
//        Log.d("Progress", String.valueOf(viewType));
//        if (viewType == VIEW_ITEM) {
//            View v = LayoutInflater.from(parent.getContext()).inflate(
//                    R.layout.kategori_item, parent, false);
//
//            vh = new ViewHolder(v);
//        } else {
//            View v = LayoutInflater.from(parent.getContext()).inflate(
//                    R.layout.progress_item, parent, false);
//
//            vh = new ProgressViewHolder(v);
//        }
//        return vh;
//    }
//
//    @Override
//    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
//
//        Log.d("flagdiadapter", String.valueOf(flagprog));
//        if (viewHolder instanceof ViewHolder) {
//
//            Typeface bold = Typeface.createFromAsset(context.getAssets(),  "Quicksand-Bold.otf");
//            ((ViewHolder) viewHolder).txtharga.setTypeface(bold);
//            ((ViewHolder) viewHolder).txtnama.setTypeface(bold);
//
//            flagprog = 1;
//            Kategori modelkategori = listkategori.get(i);
//            ((ViewHolder) viewHolder).txtnama.setText(modelkategori.getNama_kategori());
//            if(modelkategori.getJml().equalsIgnoreCase("0"))
//            {
//                ((ViewHolder) viewHolder).txtharga.setText("Tidak ada produk");
//            }
//            else {
//                ((ViewHolder) viewHolder).txtharga.setText(modelkategori.getJml() + " produk");
//            }
//            Picasso.with(context).load("http://merchant.kartuvirtual.com/assets/upload/kategori/" + modelkategori.getPhoto())
//                    .error(R.drawable.loadimage)
//                    .placeholder(R.drawable.loadimage)
//                    .into(((ViewHolder) viewHolder).imgThumbnail);
//
//            //getdata
//            ((ViewHolder) viewHolder).currentItem = listkategori.get(i);
//        }
//        else {
//
//            flagprog = 0;
//            Log.d("koneksi", String.valueOf(fragment.flagkoneksi));
//            if(fragment.flagkoneksi == 1) {
//                ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
//                ((ProgressViewHolder) viewHolder).progressBar.setVisibility(View.GONE);
//                ((ProgressViewHolder) viewHolder).img.setVisibility(View.VISIBLE);
//            }
//            else
//            {
//                ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
//                ((ProgressViewHolder) viewHolder).progressBar.setVisibility(View.VISIBLE);
//                ((ProgressViewHolder) viewHolder).img.setVisibility(View.GONE);
//            }
//
//        }
//    }
//
//    public void addItem(Kategori kategori, int index) {
//        listkategori.add(kategori);
//        notifyItemInserted(index);
//    }
//
//    public void deleteItem(int index) {
//        listkategori.remove(index);
//        notifyItemRemoved(index);
//    }
//
//    @Override
//    public int getItemCount() {
//        return listkategori.size();
//    }
//
//    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
//        this.onLoadMoreListener = onLoadMoreListener;
//    }
//
//    public void setLoaded() {
//        loading = false;
//    }
//
//    public void setProgressOffSpan() {
//        flagprog = 1;
//    }
//
//    public void setProgressOnSpan() {
//        flagprog = 0;
//    }
//
//
//    class ViewHolder extends RecyclerView.ViewHolder{
//
//        public ImageView imgThumbnail;
//        public TextView txtnama;
//        public TextView txtharga;
//
//        public Kategori currentItem;
//
//        public ViewHolder(View itemView) {
//            super(itemView);
//            imgThumbnail = (ImageView)itemView.findViewById(R.id.gambar_card);
//            txtnama = (TextView)itemView.findViewById(R.id.nama);
//            txtharga = (TextView)itemView.findViewById(R.id.harga);
//
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent i = new Intent(view.getContext(), KatalogActivity.class);
//
//                    Bundle bundle = new Bundle();
//                    bundle.putString("id_kategori",currentItem.getId_kategori());
//                    bundle.putString("nama_kategori",currentItem.getNama_kategori());
//
//                    i.putExtras(bundle);
//                    ActivityOptionsCompat options =  ActivityOptionsCompat.makeSceneTransitionAnimation(activity, txtnama, "detil" );
//
//                    ActivityCompat.startActivity(activity, i, options.toBundle());
//
//                }
//            });
//        }
//    }
//
//    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
//        public ProgressBar progressBar;
//        public ImageView img;
//
//        public ProgressViewHolder(View v) {
//            super(v);
//            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
//            img = (ImageView) v.findViewById(R.id.img);
//
//            imgprog = img;
//            progressprog = progressBar;
//
//            img.setVisibility(View.GONE);
//            img.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (onLoadMoreListener != null) {
//                        onLoadMoreListener.onLoadMore();
//                    }
//                    fragment.deleteItem();
//                    loading = true;
//                }
//            });
//        }
//    }
}



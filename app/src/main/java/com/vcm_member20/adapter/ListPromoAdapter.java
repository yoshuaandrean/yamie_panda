package com.vcm_member20.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vcm_member20.R;
import com.vcm_member20.activity.DetilPromoActivity;
import com.vcm_member20.fragment.FragmentPromo;
import com.vcm_member20.model.Post;
import com.vcm_member20.util.OnLoadMoreListener;
import com.vcm_member20.util.TimeFormater;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Edwin on 18/01/2015.
 */

public class ListPromoAdapter extends RecyclerView.Adapter {


    public final int VIEW_ITEM = 1;
    public final int VIEW_PROG = 0;

    // The minimum amount of items to have below your current scroll position
    // before loading more.
    public int visibleThreshold = 1;
    public int lastVisibleItem, totalItemCount;
    public static boolean loading;
    public static OnLoadMoreListener onLoadMoreListener;


    List<Post> mList;

    private Context context;

    private int lastPosition = -1;

    public static FragmentPromo fragment;

    public Activity activity;

    public ListPromoAdapter(Context context, ArrayList<Post> news, RecyclerView recyclerView, FragmentPromo fragment, Activity activity) {
        super();

        this.fragment = fragment;

        this.context = context;

        this.activity = activity;

        mList = news;

        Log.d("Size List", String.valueOf(mList.size()));

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold) + 1) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        Log.d("Progress", String.valueOf(viewType));
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.hotnews_item, parent, false);

            vh = new ViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolder) {

            ((ViewHolder) viewHolder).imgdiskon.setVisibility(View.GONE);
            ((ViewHolder) viewHolder).txtdiskon.setVisibility(View.GONE);
            Typeface bold = Typeface.createFromAsset(context.getAssets(),  "Quicksand-Bold.otf");
            ((ViewHolder) viewHolder).txtdiskon.setTypeface(bold);
            ((ViewHolder) viewHolder).txtnama.setTypeface(bold);
            ((ViewHolder) viewHolder).txtdesc.setTypeface(bold);
            ((ViewHolder) viewHolder).txttanggal.setTypeface(bold);

            Post model = mList.get(i);
            Picasso.with(context).load("http://kasir.yamiepanda.com/"+model.getPhoto().replace("\\", "/"))
                    .error(R.drawable.loadimage)
                    .placeholder(R.drawable.loadimage)
                    .into(((ViewHolder) viewHolder).imgThumbnail);
            ((ViewHolder) viewHolder).txtnama.setText(model.getTitle().toUpperCase());
            ((ViewHolder) viewHolder).txtdesc.setText(model.getContent());
            ((ViewHolder) viewHolder).txttanggal.setText(new TimeFormater().formattedDateFromString("yyyy-MM-dd", "dd MMMM yyyy", model.getCreated_at()));

            ((ViewHolder) viewHolder).currentItem = mList.get(i);
            setAnimation( ((ViewHolder) viewHolder).itemView, i);
        }
        else {
            if(fragment.flagkoneksi == 1) {
                ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
                ((ProgressViewHolder) viewHolder).progressBar.setVisibility(View.GONE);
                ((ProgressViewHolder) viewHolder).img.setVisibility(View.VISIBLE);
            }
            else
            {
                ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
                ((ProgressViewHolder) viewHolder).progressBar.setVisibility(View.VISIBLE);
                ((ProgressViewHolder) viewHolder).img.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onViewDetachedFromWindow(final RecyclerView.ViewHolder viewHolder)
    {
        if (viewHolder instanceof ViewHolder) {
            ((ViewHolder) viewHolder).itemView.clearAnimation();
        }
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void addItem(Post news, int index) {
        mList.add(news);
        notifyDataSetChanged();
    }

    public void deleteItem(int index) {
        mList.remove(index);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }


class ViewHolder extends RecyclerView.ViewHolder{

    public ImageView imgThumbnail, imgdiskon;
    public TextView txtnama, txtdesc, txttanggal, txtdiskon;

    public Post currentItem;

        public ViewHolder(View itemView) {
            super(itemView);
            imgThumbnail = (ImageView)itemView.findViewById(R.id.gambar_card);
            imgdiskon = (ImageView)itemView.findViewById(R.id.imgdiskon);
            txtnama = (TextView)itemView.findViewById(R.id.nama);
            txtdesc = (TextView)itemView.findViewById(R.id.deskripsi);
            txttanggal = (TextView)itemView.findViewById(R.id.tanggal);
            txtdiskon = (TextView)itemView.findViewById(R.id.txtdiskon);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(view.getContext(), DetilPromoActivity.class);

                    Bundle b = new Bundle();
                    b.putString("idnews", String.valueOf(currentItem.getId()));
                    b.putString("judul", String.valueOf(currentItem.getTitle()));
                    b.putString("isi", String.valueOf(currentItem.getContent()));
                    b.putString("photo", "http://kasir.yamiepanda.com/" + currentItem.getPhoto());
                    b.putString("merchant", String.valueOf(currentItem.getBranch_id()));
                    b.putString("tgl_lengkap", String.valueOf(currentItem.getCreated_at()));

                    i.putExtras(b);
                    context.startActivity(i);

                }
            });
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ImageView img;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
            img = (ImageView) v.findViewById(R.id.img);

            img.setVisibility(View.GONE);
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }

                    fragment.deleteItem();

                    Log.d("nyobajaa", "click");
                }
            });
        }
    }
}


